<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Penduduk extends Model
{
    protected $fillable = [
			'id_kk',
			'nik',
			'nama',
			'jenis_kelamin',
			'tempat_lahir',
			'tanggal_lahir',
			'pendidikan',
			'pekerjaan',
			'status_pernikahan',
			'status_hubungan',
			'kewarganegaraan',
			'nama_ayah',
			'nama_ibu'
		];

	protected $table = 'penduduks';

	public function getPenduduks($queriesArray, $limit, $offset){
		$tempQuery = DB::table($this->table);
		foreach($queriesArray as $key => $query){
            //build search queries
            if($query != ''){
	            if($key=='id_kk') {
					$tempQuery->where('id_kk', 'like', '%'.$query.'%');
				}
				else if($key=='nik') {
					$tempQuery->where('nik', 'like', '%'.$query.'%');
				} 
				else if($key=='nama') {
					$tempQuery->where('nama', 'like', '%'.$query.'%');
				} 
				else if($key=='jenis_kelamin') {
					$tempQuery->where('jenis_kelamin', 'like', '%'.$query.'%');
				} 
				else if($key=='tempat_lahir') {
					$tempQuery->where('tempat_lahir', 'like', '%'.$query.'%');
				} 
				else if($key=='tanggal_lahir') {
					$tempQuery->where('tanggal_lahir', 'like', '%'.$query.'%');
				} 
				else if($key=='pendidikan') {
					$tempQuery->where('pendidikan', 'like', '%'.$query.'%');
				}
				else if($key=='pekerjaan') {
					$tempQuery->where('pekerjaan', 'like', '%'.$query.'%');
				}
				else if($key=='status_pernikahan') {
					$tempQuery->where('status_pernikahan', 'like', '%'.$query.'%');
				}
				else if($key=='status_hubungan') {
					$tempQuery->where('status_hubungan', 'like', '%'.$query.'%');
				}
				else if($key=='kewarganegaraan') {
					$tempQuery->where('kewarganegaraan', 'like', '%'.$query.'%');
				}
				else if($key=='nama_ayah') {
					$tempQuery->where('nama_ayah', 'like', '%'.$query.'%');
				}
				else if($key=='nama_ibu') {
					$tempQuery->where('nama_ibu', 'like', '%'.$query.'%');
				}
			}
        }
        
        $data['sql'] = $tempQuery->toSql();
        $data['count'] = $tempQuery->count();
        $data['result'] = $tempQuery->limit($limit)->offset($offset)->get();
        return $data;
    }
}
