<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use App\Penduduk;
use App\KartuKeluarga;
use App\Pekerjaan;
use DB;
use DataTables;

class PendudukController extends Controller
{
    private $logAction;

    public function __construct()
    {

      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = 'Penduduk';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penduduk = Penduduk::all();

        return view('admin.data_penduduk.list')
            ->with('dk', $penduduk);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $penduduk = New Penduduk;
        $action = "data_penduduk.store";
        $title = "Tambah Data Penduduk";

        return view('admin.data_penduduk.form')
            ->with('penduduk', $penduduk)
            ->with('action', $action)
            ->with('title', $title);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $penduduk = new Penduduk;

        $this->validate($request, [
            'noKK' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'jk' => 'required',
            'tempatLahir' => 'required',
            'tanggalLahir' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'statusPernikahan' => 'required',
            'statusHubungan' => 'required',
            'kewarganegaraan' => 'required',
            'namaAyah' => 'required',
            'namaIbu' => 'required',
            'agama' => 'required'
        ]);

        $penduduk->id_kk = $request->noKK;
        $penduduk->nik = $request->nik;
        $penduduk->nama = $request->nama;
        $penduduk->jenis_kelamin = $request->jk;
        $penduduk->tempat_lahir = $request->tempatLahir;
        $penduduk->tanggal_lahir = date("Y-m-d", strtotime($request->tanggalLahir));
        $penduduk->pendidikan = $request->pendidikan;
        $penduduk->pekerjaan = $request->pekerjaan;
        $penduduk->agama = $request->agama;
        $penduduk->status_pernikahan = $request->statusPernikahan;
        $penduduk->status_hubungan = $request->statusHubungan;
        $penduduk->kewarganegaraan = $request->kewarganegaraan;
        $penduduk->nama_ayah = $request->namaAyah;
        $penduduk->nama_ibu = $request->namaIbu;

        $this->logAction->action = "Tambah Data Penduduk - ".$request->nik;

      try{
            $penduduk->save();
            $this->logAction->item_id = $penduduk->id;
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('data_penduduk.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penduduk = Penduduk::find($id);
        $action = "data_penduduk.update";
        $title = "Edit Data Penduduk";

        return view('admin.data_penduduk.form')
            ->with('penduduk', $penduduk)
            ->with('action', $action)
            ->with('title', $title);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $penduduk = Penduduk::find($id);

        $this->validate($request, [
            'noKK' => 'required',
            'nik' => 'required',
            'nama' => 'required',
            'jk' => 'required',
            'tempatLahir' => 'required',
            'tanggalLahir' => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'statusPernikahan' => 'required',
            'statusHubungan' => 'required',
            'kewarganegaraan' => 'required',
            'namaAyah' => 'required',
            'namaIbu' => 'required',
            'agama' => 'required',
        ]);
        
        $penduduk->id_kk = $request->noKK;
        $penduduk->nik = $request->nik;
        $penduduk->nama = $request->nama;
        $penduduk->jenis_kelamin = $request->jk;
        $penduduk->tempat_lahir = $request->tempatLahir;
        $penduduk->tanggal_lahir = date("Y-m-d", strtotime($request->tanggalLahir));
        $penduduk->pendidikan = $request->pendidikan;
        $penduduk->pekerjaan = $request->pekerjaan;
        $penduduk->agama = $request->agama;
        $penduduk->status_pernikahan = $request->statusPernikahan;
        $penduduk->status_hubungan = $request->statusHubungan;
        $penduduk->kewarganegaraan = $request->kewarganegaraan;
        $penduduk->nama_ayah = $request->namaAyah;
        $penduduk->nama_ibu = $request->namaIbu;

        $this->logAction->action = "Edit Data Penduduk - ".$request->nik;

        try{
            $penduduk->save();
            $this->logAction->item_id = $penduduk->id;
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('data_penduduk.index'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penduduk = Penduduk::find($id);

        $this->logAction->action = "Hapus Data Penduduk - ".$penduduk->nik;

        try{
            Penduduk::destroy($id);
            $this->logAction->item_id = $penduduk->id;
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('data_penduduk.index');
    }

    /**
     * Retrieve kk by query via ajax, limit : 5 
     *
     * @param  _POST  query
     * @return kk list json
     */
    public function getPenduduk(){
        $penduduks = Penduduk::where('nik', 'like', '%'.$_GET['q'].'%')
                ->orWhere('nama', 'like', '%'.$_GET['q'].'%')
                ->limit(5)->get();

        return response()->json($penduduks);
    }

    /**
     * Retrieve kk by query via ajax, limit : 5 
     *
     * @param  _POST  query
     * @return kk list json
     */
    public function getIndividu(){
        $individu = Penduduk::where('nik', 'like', '%'.$_GET['q'].'%')
                ->orWhere('nama', 'like', '%'.$_GET['q'].'%')
                ->Where('id_kk', 'like', '%'.$_GET['kk'].'%')
                ->limit(5)->get();

        return response()->json($individu);
    }

    public function print(Request $request){
        $q = DB::table('penduduks')->select(['penduduks.id', 'nik', 'penduduks.nama','jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'pendidikan', 'pekerjaan', 'agama', 'status_pernikahan','status_hubungan', 'kewarganegaraan', 'penduduks.created_at', 'nama_ayah', 'nama_ibu', 'id_kk', 'kartu_keluargas.alamat', 'kartu_keluargas.no_kk', 'kartu_keluargas.kepala_keluarga', 'kartu_keluargas.rt', 'kartu_keluargas.rw', 'kartu_keluargas.kelurahan'])
        ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'penduduks.id_kk');

        if($request->has('nokk'))
        {
            $q->where('kartu_keluargas.no_kk', 'like', "%{$request->nokk}%");
        }
        if($request->has('kepalakeluarga'))
        {
            $q->where('kartu_keluargas.kepala_keluarga', 'like', "%{$request->kepalakeluarga}%");
        }
        if($request->has('nama'))
        {
            $q->where('penduduks.nama', 'like', "%{$request->nama}%");
        }
        if($request->has('nik'))
        {
            $q->where('nik', 'like', "%{$request->nik}%");
        }
        if($request->has('tempatlahir'))
        {
            $q->where('tempat_lahir', 'like', "%{$request->tempatlahir}%");
        }
        if($request->has('lahirawal'))
        {
            $lstart = date("Y-m-d", strtotime($request->lahirawal));
            $q->where('tanggal_lahir', '>', $lstart);
        }
        if($request->has('lahirakhir'))
        {
            $lend = date("Y-m-d", strtotime($request->lahirakhir));
            $q->where('tanggal_lahir', '<', $lend);
        }
        if($request->has('ayah'))
        {
            $q->where('nama_ayah', 'like', "%{$request->ayah}%");
        }
        if($request->has('ibu'))
        {
            $q->where('nama_ibu', 'like', "%{$request->ibu}%");
        }
        if($request->has('jk') && $request->jk != "Semua")
        {
            $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
        }
        if($request->has('kewarganegaraan') && $request->kewarganegaraan != "Semua")
        {
            $q->where('kewarganegaraan', '=', $request->kewarganegaraan);
        }
        if($request->has('agama') && $request->agama != "Semua")
        {
            $q->where('agama', 'like', "%{$request->agama}%");
        }
        if($request->has('pendidikan') && $request->pendidikan != "Semua")
        {
            $q->where('pendidikan', '=', $request->pendidikan);
        }
        if($request->has('pekerjaan') && $request->pekerjaan != "Semua")
        {
            $q->where('penduduks.pekerjaan', '=', $request->pekerjaan);
        }
        if($request->has('statusHubungan') && $request->statusHubungan != "Semua")
        {
            $q->where('status_hubungan', '=', $request->statusHubungan);
        }
        if($request->has('statusPernikahan') && $request->statusPernikahan != "Semua")
        {
            $q->where('status_pernikahan', '=', $request->statusPernikahan);
        }
        if($request->has('tglAwal'))
        {
            $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
            $q->where('penduduks.created_at', '>', $start);
        }
        if($request->has('tglAkhir'))
        {
            $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
            $q->where('penduduks.created_at', '<', $end);
        }
        if($request->has('kelurahan') && $request->kelurahan != "Semua")
        {
            $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
        }
        if($request->has('alamat'))
        {
            $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
        }
        if($request->has('rt'))
        {
            $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
        }
        if($request->has('rw'))
        {
            $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
        }

        $penduduk = $q->get();
        $count = $q->count();

        return view('admin.data_penduduk.print')
            ->with('penduduk', $penduduk)
            ->with('filter', $request)
            ->with('count', $count)
            ->with('typeLaporan', 'Data Penduduk');
    }
    /**
     * Retrieve offices by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */
    public function getDatatablePenduduks(Request $request){        
      $penduduks = DB::table('penduduks')->select(['penduduks.id', 'nik', 'penduduks.nama','jenis_kelamin', 'tempat_lahir', 'tanggal_lahir', 'pendidikan', 'pekerjaan', 'agama', 'status_pernikahan','status_hubungan', 'kewarganegaraan', 'penduduks.created_at', 'nama_ayah', 'nama_ibu', 'id_kk', 'kartu_keluargas.alamat', 'kartu_keluargas.no_kk', 'kartu_keluargas.kepala_keluarga', 'kartu_keluargas.rt', 'kartu_keluargas.rw', 'kartu_keluargas.kelurahan', 'penduduks.created_at'])
        ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'penduduks.id_kk');

      return DataTables::of($penduduks)
        ->filter(function($q) use ($request){
            if($request->has('nokk'))
            {
                $q->where('kartu_keluargas.no_kk', 'like', "%{$request->nokk}%");
            }
            if($request->has('kepalakeluarga'))
            {
                $q->where('kartu_keluargas.kepala_keluarga', 'like', "%{$request->kepalakeluarga}%");
            }
            if($request->has('nama'))
            {
                $q->where('penduduks.nama', 'like', "%{$request->nama}%");
            }
            if($request->has('nik'))
            {
                $q->where('nik', 'like', "%{$request->nik}%");
            }
            if($request->has('tempatlahir'))
            {
                $q->where('tempat_lahir', 'like', "%{$request->tempatlahir}%");
            }
            if($request->has('lahirawal'))
            {
                $lstart = date("Y-m-d", strtotime($request->lahirawal));
                $q->where('tanggal_lahir', '>', $lstart);
            }
            if($request->has('lahirakhir'))
            {
                $lend = date("Y-m-d", strtotime($request->lahirakhir));
                $q->where('tanggal_lahir', '<', $lend);
            }
            if($request->has('ayah'))
            {
                $q->where('nama_ayah', 'like', "%{$request->ayah}%");
            }
            if($request->has('ibu'))
            {
                $q->where('nama_ibu', 'like', "%{$request->ibu}%");
            }
            if($request->has('jk') && $request->jk != "Semua")
            {
                $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
            }
            if($request->has('kewarganegaraan') && $request->kewarganegaraan != "Semua")
            {
                $q->where('kewarganegaraan', '=', $request->kewarganegaraan);
            }
            if($request->has('agama') && $request->agama != "Semua")
            {
                $q->where('agama', 'like', "%{$request->agama}%");
            }
            if($request->has('pendidikan') && $request->pendidikan != "Semua")
            {
                $q->where('pendidikan', '=', $request->pendidikan);
            }
            if($request->has('pekerjaan') && $request->pekerjaan != "Semua")
            {
                $q->where('penduduks.pekerjaan', '=', $request->pekerjaan);
            }
            if($request->has('statusHubungan') && $request->statusHubungan != "Semua")
            {
                $q->where('status_hubungan', '=', $request->statusHubungan);
            }
            if($request->has('statusPernikahan') && $request->statusPernikahan != "Semua")
            {
                $q->where('status_pernikahan', '=', $request->statusPernikahan);
            }
            if($request->has('tglAwal'))
            {
                $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
                $q->where('penduduks.created_at', '>', $start);
            }
            if($request->has('tglAkhir'))
            {
                $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
                $q->where('penduduks.created_at', '<', $end);
            }
            if($request->has('kelurahan') && $request->kelurahan != "Semua")
            {
                $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
            }
            if($request->has('alamat'))
            {
                $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
            }
            if($request->has('rt'))
            {
                $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
            }
            if($request->has('rw'))
            {
                $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
            }
        })
        ->addColumn('action', function($data){
            return("
                <a href='".url('admin/master/data_penduduk')."/".$data->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$data->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->addColumn('alamat', function($data){
            $kk = KartuKeluarga::find($data->id_kk);
            return($kk->alamat.' RT '.$kk->rt.' RW '.$kk->rw);
        })
        ->addColumn('ttl', function($data){
            $tanggal = date('d/m/Y', strtotime($data->tanggal_lahir));
            return($data->tempat_lahir.', '.$tanggal);
        })
        ->addColumn('ortu', function($data){
            return($data->nama_ayah.' - '.$data->nama_ibu);
        })
        ->addColumn('kelurahan', function($data){
            $kk = KartuKeluarga::find($data->id_kk);
            return($kk->kelurahan);
        })
        ->editColumn('jenis_kelamin', function($data){
            if($data->jenis_kelamin == "Laki - Laki")
            {
                return("L");
            }
            elseif($data->jenis_kelamin == "Perempuan"){
                return("P");
            }
            else{
                return $data->jenis_kelamin;
            }
        })
        ->editColumn('created_at', function($data){
            $d = date("d/m/Y", strtotime($data->created_at));
            return($d);
        })
        ->make(true);
    }
}
