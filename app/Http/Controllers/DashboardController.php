<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KartuKeluarga;
use App\Penduduk;
use App\Kelahiran;
use App\Kematian;
use App\Pindah;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $s = Carbon::now()->startOfMonth();
        $e = Carbon::now()->endOfMonth();

        $c = array();
        $cDP = array();
        $cKK = array();
        $chart = array();

        $kk = KartuKeluarga::where('created_at', '>', $s)->where('created_at', '<', $e)->count();
        $penduduk = Penduduk::where('created_at', '>', $s)->where('created_at', '<', $e)->count();
        $kelahiran = Kelahiran::where('created_at', '>', $s)->where('created_at', '<', $e)->count();
        $kematian = Kematian::where('created_at', '>', $s)->where('created_at', '<', $e)->count();
        $datang = Pindah::where('created_at', '>', $s)->where('created_at', '<', $e)->where('jenis', '=', 'datang')->count();
        $pergi = Pindah::where('created_at', '>', $s)->where('created_at', '<', $e)->where('jenis', '=', 'pergi')->count();

        $kelurahan = DB::table('kartu_keluargas')->distinct('kelurahan')->orderBy('kelurahan', 'ASC')->get();
        $kartuKeluarga = DB::table('kartu_keluargas')->whereBetween('created_at', [$s, $e])->get();
        $dataPenduduk = DB::table('penduduks')->rightJoin('kartu_keluargas', 'penduduks.id_kk', 'kartu_keluargas.id')->whereBetween('penduduks.created_at', [$s, $e])->get();
        $dataKelahiran = DB::table('kelahirans')->join('kartu_keluargas', 'kelahirans.id_kk', 'kartu_keluargas.id')->whereBetween('kelahirans.created_at', [$s, $e])->get();
        $dataKematian = DB::table('kematians')->join('penduduks', 'kematians.id_ktp', 'penduduks.id')->join('kartu_keluargas', 'kartu_keluargas.id', 'kematians.id_kk')->whereBetween('kematians.created_at', [$s, $e])->get();
        $dataPindahDatang = DB::table('pindahs')->where('jenis', 'datang')->whereBetween('created_at', [$s, $e])->get();
        $dataPindahPergi = DB::table('pindahs')->where('jenis', 'pergi')->whereBetween('created_at', [$s, $e])->get();

        foreach($kelurahan as $data){
            $chart[$data->kelurahan]['KK'] = 0;
            $chart[$data->kelurahan]['DP'] = 0;
            $chart[$data->kelurahan]['KLR'] = 0;
            $chart[$data->kelurahan]['KMT'] = 0;
            $chart[$data->kelurahan]['DTG'] = 0;
            $chart[$data->kelurahan]['PRG'] = 0;
        }
        foreach($kartuKeluarga as $data){
            $chart[$data->kelurahan]['KK']++;
        }
        foreach($dataPenduduk as $data){
            $chart[$data->kelurahan]['DP']++;
        }
        foreach($dataKelahiran as $data){
            $chart[$data->kelurahan]['KLR']++;
        }
        foreach($dataKematian as $data){
            $chart[$data->kelurahan]['KMT']++;
        }
        foreach($dataPindahDatang as $data){
            $chart[$data->kelurahan]['DTG']++;
        }
        foreach($dataPindahPergi as $data){
            $chart[$data->kelurahan]['PRG']++;
        }

        foreach($chart as $key=>$data){
            $cKK[] = $data['KK'];
            $cDP[] = $data['DP'];
            $c[] = $data['KLR'] - $data['KMT'] + $data['DTG'] - $data['PRG'];
        }

        return view('admin.dashboard')
            ->with('kk', $kk)
            ->with('penduduk', $penduduk)
            ->with('kelahiran', $kelahiran)
            ->with('kematian', $kematian)
            ->with('datang', $datang)
            ->with('pergi', $pergi)
            ->with('c', $c)
            ->with('cKK', $cKK)
            ->with('cDP', $cDP);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
