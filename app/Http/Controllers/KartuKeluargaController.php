<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use Illuminate\Support\Facades\Auth;
use App\ActionLog;
use App\KartuKeluarga;
use DB;
use DataTables;


class KartuKeluargaController extends Controller
{
    private $KKAction;

    public function __construct()
    {
      $this->KKAction = New ActionLog;
      $this->KKAction->id_user = 1; //should be changed
      $this->KKAction->group = "Kartu Keluarga";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_kk = KartuKeluarga::all();

        return view('admin.kartu_keluarga.list')
            ->with('data_kk', $data_kk);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kk = New KartuKeluarga;
        $title = "Tambah Data Kartu Keluarga";
        $action = "kartu_keluarga.store";

        return view('admin.kartu_keluarga.form')
            ->with('kk', $kk)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kk = new KartuKeluarga;        

        $this->validate($request, [
            'noKK' => 'required',
            'kepalaKeluarga' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required'
        ]);        
        
        $kk->no_kk = $request->noKK;
        $kk->kepala_keluarga = $request->kepalaKeluarga;
        $kk->alamat = $request->alamat;
        $kk->rt = $request->rt;
        $kk->rw = $request->rw;
        $kk->kelurahan = $request->kelurahan;

        $this->KKAction->action = "Tambah Data Kartu Keluarga - ".$request->noKK;
        
        try{
            $kk->save();
            $this->KKAction->item_id = $kk->id;
            $this->KKAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kartu_keluarga.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kk = KartuKeluarga::find($id);
        $title = "Edit Data - ".$kk->no_kk;
        $action = "kartu_keluarga.update";

        return view('admin.kartu_keluarga.form')
            ->with('kk', $kk)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kk = KartuKeluarga::find($id);        

        $this->validate($request, [
            'noKK' => 'required',
            'kepalaKeluarga' => 'required',
            'alamat' => 'required',
            'rt' => 'required',
            'rw' => 'required',
            'kelurahan' => 'required'
        ]);        
        
        $kk->no_kk = $request->noKK;
        $kk->kepala_keluarga = $request->kepalaKeluarga;
        $kk->alamat = $request->alamat;
        $kk->rt = $request->rt;
        $kk->rw = $request->rw;
        $kk->kelurahan = $request->kelurahan;

        $this->KKAction->action = "Edit Data Kartu Keluarga - ".$request->noKK;
        
        try{
            $kk->save();
            $this->KKAction->item_id = $kk->id;
            $this->KKAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kartu_keluarga.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $kk = KartuKeluarga::find($id);

        $this->KKAction->action = "Hapus Data Kartu Keluarga - ".$kk->no_kk;

        try{
            $this->KKAction->item_id = $kk->id;
            KartuKeluarga::destroy($id);
            $this->KKAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('kartu_keluarga.index');
    }

    /**
     * Retrieve kk by query via ajax, limit : 5 
     *
     * @param  _POST  query
     * @return kk list json
     */
    public function getKKs(){
        $kks = KartuKeluarga::where('no_kk', 'like', '%'.$_GET['q'].'%')
                ->orWhere('kepala_keluarga', 'like', '%'.$_GET['q'].'%')
                ->limit(5)->get();

        return response()->json($kks);
    }

    public function print(Request $request){
      $q = DB::table('kartu_keluargas')->select(['id', 'no_kk', 'kepala_keluarga', 'alamat', 'rt', 'rw', 'kelurahan', 'created_at']);

      if($request->has('nama'))
      {
          $q->where('kepala_keluarga', 'like', "%{$request->nama}%");
      }
      if($request->has('nokk'))
      {
          $q->where('no_kk', 'like', "%{$request->nokk}%");
      }
      if($request->has('tglAwal'))
      {
          $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
          $q->where('created_at', '>', $start);
      }
      if($request->has('tglAkhir'))
      {
          $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
          $q->where('created_at', '<', $end);
      }
      if($request->kelurahan != "Semua")
      {
          $q->where('kelurahan', 'like', "%{$request->kelurahan}%");
      }
      if($request->has('alamat'))
      {
          $q->where('alamat', 'like', "%{$request->alamat}%");
      }
      if($request->has('rt'))
      {
          $q->where('rt', 'like', "%{$request->rt}%");
      }
      if($request->has('rw'))
      {
          $q->where('rw', 'like', "%{$request->rw}%");
      }

      $kks = $q->get();
      $count = $q->count();

      return view('admin.kartu_keluarga.print')
        ->with('kk', $kks)
        ->with('filter', $request)
        ->with('count', $count)
        ->with('typeLaporan', 'Data Kartu Keluarga');
    }
    /**
     * Retrieve offices by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */
    public function getDatatableKKs(Request $request){
      $kks = DB::table('kartu_keluargas')->select(['id', 'no_kk', 'kepala_keluarga', 'alamat', 'rt', 'rw', 'kelurahan', 'created_at']);

      return DataTables::of($kks)
        ->filter(function($q) use ($request){
            if($request->has('nama'))
            {
                $q->where('kepala_keluarga', 'like', "%{$request->nama}%");
            }
            if($request->has('nokk'))
            {
                $q->where('no_kk', 'like', "%{$request->nokk}%");
            }
            if($request->has('tglAwal'))
            {
                $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
                $q->where('created_at', '>', $start);
            }
            if($request->has('tglAkhir'))
            {
                $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
                $q->where('created_at', '<', $end);
            }
            if($request->kelurahan != "Semua")
            {
                $q->where('kelurahan', 'like', "%{$request->kelurahan}%");
            }
            if($request->has('alamat'))
            {
                $q->where('alamat', 'like', "%{$request->alamat}%");
            }
            if($request->has('rt'))
            {
                $q->where('rt', 'like', "%{$request->rt}%");
            }
            if($request->has('rw'))
            {
                $q->where('rw', 'like', "%{$request->rw}%");
            }
        })
        ->addColumn('action', function($kk){
            return("
                <a href='".url('admin/master/kartu_keluarga')."/".$kk->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$kk->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->editColumn('alamat', function($kk){
          return($kk->alamat.' RT '.$kk->rt.' RW '.$kk->rw);
        })
        ->editColumn('created_at', function($kk){
          $d = date("d/m/Y", strtotime($kk->created_at));
          return($d);
        })
        ->make(true);
    }
}
