<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use DB;
use DataTables;
use Carbon\Carbon;


class ActionLogController extends Controller
{
    public function index()
    {
        return view('admin.actionlog');
    }

    public function getDatatableLogs(Request $request){
      $data = DB::table('action_logs')->select(['action_logs.id', 'id_user', 'action', 'group', 'item_id', 'action_logs.created_at', 'username as u_username'])
        ->leftJoin('users', 'id_user', 'users.id')
        ->orderBy('created_at', 'DESC');

      return DataTables::of($data)
      ->editColumn('created_at', function($d){
            $y = date("Y", strtotime($d->created_at));
            $m = date("m", strtotime($d->created_at));
            $day = date("d", strtotime($d->created_at));
            $hour = date("H", strtotime($d->created_at));
            $min = date("i", strtotime($d->created_at));
            $sec = date("s", strtotime($d->created_at));
            $time    = Carbon::create($y, $m, $day, $hour, $min, $sec, 'Asia/Jakarta')->diffForHumans();
        return($time);
      })
      ->editColumn('id_user', function($d){
        return($d->u_username);
      })
      ->make(true);
    }
}
