<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Html\HtmlServiceProvider;
use Illuminate\Html\HtmlFacade;
use App\ActionLog;
use App\Kematian;
use App\KartuKeluarga;
use App\Penduduk;
use DB;
use DataTables;

class KematianController extends Controller
{   
    private $logAction;

    public function __construct()
    {

      $this->logAction = New ActionLog;
      $this->logAction->id_user = 1; //should be changed
      $this->logAction->group = "Kematian"; //should be changed
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.data_kematian.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kematian = New Kematian;
        $title = "Tambah Data Kematian";
        $action = "kematian.store";

        return view('admin.data_kematian.form')
            ->with('kematian', $kematian)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $kematian = new Kematian;        

        $this->validate($request, [
            'noSkk' => 'required',
            'idKK' => 'required',
            'idKTP' => 'required',
            'waktuKematian' => 'required'
        ]);        
        
        $kematian->no_skk = $request->noSkk;
        $kematian->id_kk = $request->idKK;
        $kematian->id_ktp = $request->idKTP;
        $kematian->status = 1;
        $kematian->waktu_kematian =  date("Y-m-d H:i:s", strtotime($request->waktuKematian));

        $this->logAction->action = "Tambah Data kematian- ".$request->noSkk;
        
        try{
            $kematian->save();
            $this->logAction->item_id = $kematian->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kematian = Kematian::find($id);
        $dataKK = KartuKeluarga::find($kematian->id_kk);
        $dataKTP = Penduduk::find($kematian->id_ktp);
        $title = "Edit Data kematian";
        $action = "kematian.update";

        return view('admin.data_kematian.form')
            ->with('kematian', $kematian)
            ->with('dataKK', $dataKK)
            ->with('dataKTP', $dataKTP)
            ->with('title', $title)
            ->with('action', $action);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kematian = Kematian::find($id);        

        $this->validate($request, [
            'noSkk' => 'required',
            'idKK' => 'required',
            'idKTP' => 'required',
            'waktuKematian' => 'required'
        ]);        
        
        $kematian->no_skk = $request->noSkk; 
        $kematian->id_kk = $request->idKK;
        $kematian->id_ktp = $request->idKTP;
        $kematian->waktu_kematian =  date("Y-m-d H:i:s", strtotime($request->waktuKematian));

        $this->logAction->action = "Ubah Data kematian- ".$request->noSkk;
        
        try{
            $kematian->save();
            $this->logAction->item_id = $kematian->id; 
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil disimpan';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat disimpan';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kematian = Kematian::find($id);

        $this->logAction->action = "Hapus Data kematian - ".$kematian->no_skk;

        try{
            $this->logAction->item_id = $kematian->id; 
            Kematian::destroy($id);
            $this->logAction->save(); 
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil dihapus';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat dihapus';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }

    public function print(Request $request){

      $q = DB::table('kematians')->select(['kematians.id', 'no_skk', 'kematians.id_kk', 'waktu_kematian', 'id_ktp', 'kartu_keluargas.id', 'alamat', 'rt', 'rw', 'kelurahan', 'nama', 'nik', 'jenis_kelamin', 'agama', 'tempat_lahir', 'status'])
        ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'kematians.id_kk')
        ->join('penduduks', 'penduduks.id', '=', 'kematians.id_ktp');

        if($request->has('nama'))
        {
            $q->where('nama', 'like', "%{$request->nama}%");
        }
        if($request->has('skk'))
        {
            $q->where('no_skk', 'like', "%{$request->skk}%");
        }
        if($request->has('tglAwal'))
        {
            $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
            $q->where('kematians.created_at', '>', $start);
        }
        if($request->has('tglAkhir'))
        {
            $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
            $q->where('kematians.created_at', '<', $end);
        }
        if($request->has('tempatLahir'))
        {
            $q->where('tempat_lahir', 'like', "%{$request->tempatLahir}%");
        }
        if($request->has('lhrAwal'))
        {
            $lStart = date("Y-m-d", strtotime($request->lhrAwal));
            $q->where('tanggal_lahir', '>', $lStart);
        }
        if($request->has('lhrAkhir'))
        {
            $lEnd = date("Y-m-d", strtotime($request->lhrAkhir));
            $q->where('tanggal_lahir', '<', $lEnd);
        }
        if($request->has('matiAwal'))
        {
            $mStart = date("Y-m-d 00:00:00", strtotime($request->matiAwal));
            $q->where('waktu_kematian', '>', $mStart);
        }
        if($request->has('matiAkhir'))
        {
            $mEnd = date("Y-m-d 23:59:59", strtotime($request->matiAkhir));
            $q->where('waktu_kematian', '<', $mEnd);
        }
        if($request->jk != "Semua")
        {
            $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
        }
        if($request->warga != "Semua")
        {
            $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
        }
        if($request->agama != "Semua")
        {
            $q->where('agama', 'like', "%{$request->agama}%");
        }
        if($request->kelurahan != "Semua")
        {
            $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
        }
        if($request->has('alamat'))
        {
            $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
        }
        if($request->has('rt'))
        {
            $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
        }
        if($request->has('rw'))
        {
            $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
        }
        if($request->has('status') && $request->status != "Semua")
        {
            $q->where('status', '=', $request->status);
        }
        $kmt = $q->get();
        $count = $q->count();

        return view('admin.data_kematian.print')
            ->with('kmt', $kmt)
            ->with('filter', $request)
            ->with('count', $count)
            ->with('typeLaporan', 'Data Kematian');

    }
    /**
     * Retrieve Kematian by query via ajax for datatables 
     *
     * @param   datatable default JSON output
     * @return  datatable JSON input
        int draw //page
        int recordsTotal //total records in database
        int recordsFiltered //
        string error (optional) //error message
     */
    public function getDatatableKematians(Request $request){
      $kematians = DB::table('kematians')->select(['kematians.id as kematian_id', 'no_skk', 'kematians.id_kk', 'waktu_kematian', 'id_ktp', 'kartu_keluargas.id', 'alamat', 'rt', 'rw', 'kelurahan', 'nama', 'nik', 'jenis_kelamin', 'agama', 'tempat_lahir', 'status'])
        ->join('kartu_keluargas', 'kartu_keluargas.id', '=', 'kematians.id_kk')
        ->join('penduduks', 'penduduks.id', '=', 'kematians.id_ktp');

      return DataTables::of($kematians)
      ->filter(function($q) use ($request){
            if($request->has('nama'))
            {
                $q->where('nama', 'like', "%{$request->nama}%");
            }
            if($request->has('skk'))
            {
                $q->where('no_skk', 'like', "%{$request->skk}%");
            }
            if($request->has('tglAwal'))
            {
                $start = date("Y-m-d 00:00:00", strtotime($request->tglAwal));
                $q->where('kematians.created_at', '>', $start);
            }
            if($request->has('tglAkhir'))
            {
                $end = date("Y-m-d 23:59:59", strtotime($request->tglAkhir));
                $q->where('kematians.created_at', '<', $end);
            }
            if($request->has('tempatLahir'))
            {
                $q->where('tempat_lahir', 'like', "%{$request->tempatLahir}%");
            }
            if($request->has('lhrAwal'))
            {
                $lStart = date("Y-m-d", strtotime($request->lhrAwal));
                $q->where('tanggal_lahir', '>', $lStart);
            }
            if($request->has('lhrAkhir'))
            {
                $lEnd = date("Y-m-d", strtotime($request->lhrAkhir));
                $q->where('tanggal_lahir', '<', $lEnd);
            }
            if($request->has('matiAwal'))
            {
                $mStart = date("Y-m-d 00:00:00", strtotime($request->matiAwal));
                $q->where('waktu_kematian', '>', $mStart);
            }
            if($request->has('matiAkhir'))
            {
                $mEnd = date("Y-m-d 23:59:59", strtotime($request->matiAkhir));
                $q->where('waktu_kematian', '<', $mEnd);
            }
            if($request->jk != "Semua")
            {
                $q->where('jenis_kelamin', 'like', "%{$request->jk}%");
            }
            if($request->warga != "Semua")
            {
                $q->where('kewarganegaraan', 'like', "%{$request->warga}%");
            }
            if($request->agama != "Semua")
            {
                $q->where('agama', 'like', "%{$request->agama}%");
            }
            if($request->kelurahan != "Semua")
            {
                $q->where('kartu_keluargas.kelurahan', 'like', "%{$request->kelurahan}%");
            }
            if($request->has('alamat'))
            {
                $q->where('kartu_keluargas.alamat', 'like', "%{$request->alamat}%");
            }
            if($request->has('rt'))
            {
                $q->where('kartu_keluargas.rt', 'like', "%{$request->rt}%");
            }
            if($request->has('rw'))
            {
                $q->where('kartu_keluargas.rw', 'like', "%{$request->rw}%");
            }
            if($request->has('status') && $request->status != "Semua")
            {
                $q->where('status', '=', $request->status);
            }
        })
        ->addColumn('action', function($k){
            if($k->status == 0){
                $s0 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/pending_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Tunda' disabled><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($k->status == 1){
                $s0 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/process_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Proses' disabled><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/finish_document' class='btn btn-xs btn-icon btn-success' data-toggle='tooltip' title='' data-original-title='Selesai'><i class='fa fa-check'></i></a>";
            }
            if($k->status == 2){
                $s0 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/pending_document' class='btn btn-xs btn-icon btn-danger' data-toggle='tooltip' title='' data-original-title='Tunda'><i class='fa fa-history'></i></a>";
                $s1 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/process_document' class='btn btn-xs btn-icon btn-warning' data-toggle='tooltip' title='' data-original-title='Proses'><i class='fa fa-file-o'></i></a>";
                $s2 = "<a href='".url("admin/kematian")."/".$k->kematian_id."/finish_document' class='btn btn-xs btn-icon btn-default' data-toggle='tooltip' title='' data-original-title='Selesai' disabled><i class='fa fa-check'></i></a>";
            }             
            return("$s0 $s1 $s2
                <a href='".url('admin/kematian')."/".$k->id."/edit' class='btn btn-icon btn-xs btn-info' data-toggle='tooltip' title='' data-original-title='Edit Data'>
                    <i class='fa fa-edit'></i>
                </a>                               
                <a deleteId='".$k->id."' class='btn btn-icon btn-xs deleteButton' data-toggle='tooltip' title='' data-original-title='Hapus Data' style='background-color: #4527A0; color: white;'>
                    <i class='fa fa-trash'></i>
                </a>");
        })
        ->addColumn('alamat', function($k){
            $kk = KartuKeluarga::find($k->id_kk);
             return($kk->alamat.' RT '.$kk->rt.' RW '.$kk->rw);
        })
        ->addColumn('nama', function($k){
            $penduduk = Penduduk::find($k->id_ktp);
            if($penduduk->jenis_kelamin == 'Laki - Laki')
                $jk = "L";
            else
                $jk = "P";

            return($penduduk->nama.' - '.'('.$jk.')');
        })
        ->addColumn('kelurahan', function($k){
            $kk = KartuKeluarga::find($k->id_kk);
            return($kk->kelurahan);
        })
        ->editColumn('waktu_kematian', function($k){
            return date("d/m/Y H:i", strtotime($k->waktu_kematian));
        })
        ->editColumn('status', function($k){
            if($k->status == 0)
                return("<span class='label label-danger'>Ditunda</span>");
            if($k->status == 1)
                return("<span class='label label-warning'>Diproses</span>");
            if($k->status == 2)
                return("<span class='label label-success'>Selesai</span>");
        })
        ->rawColumns(['status', 'action'])
        ->make(true);
    }

    public function pendingDoc($id)
    {      
        $kematian = Kematian::find($id);
        $this->logAction->action = "Update Status Data Kematian [Tunda] - ".$kematian->no_skk;

        try{
            $kematian->status = 0;
            $kematian->save();
            $this->logAction->item_id = $kematian->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }

    public function processDoc($id)
    {
        $kematian = Kematian::find($id);
        $this->logAction->action = "Update Status Data kematian [Proses] - ".$kematian->no_skk;

        try{
            $kematian->status = 1;
            $kematian->save();
            $this->logAction->item_id = $kematian->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }

    public function finishDoc($id)
    {
        $kematian = Kematian::find($id);
        $this->logAction->action = "Update Status Data kematian [Selesai] - ".$kematian->no_skk;

        try{
            $kematian->status = 2;
            $kematian->save();
            $this->logAction->item_id = $kematian->id;
            $this->logAction->save();
            $message['title'] = 'Berhasil';
            $message['type'] = 'success';
            $message['text'] = 'Data berhasil diupdate';
            session()->flash('status', $message);
        } 
        catch(\Exception $e)
        {
            $message['title'] = 'Gagal';
            $message['type'] = 'error';
            $message['text'] = 'Data tidak dapat diupdate';
            session()->flash('status', $message);
        }

        return redirect()->route('kematian.index');
    }
}
