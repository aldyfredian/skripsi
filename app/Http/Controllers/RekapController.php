<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class RekapController extends Controller
{
    public function index()
    {
        return view('admin.rekap-form')
            ->with('title', 'Laporan Rekapitulasi Data');
    }

    public function print(Request $request)
    {
        // Table
        $kelurahan = DB::table('kartu_keluargas')->distinct('kelurahan')->orderBy('kelurahan', 'ASC')->get();
        $kartuKeluarga = DB::table('kartu_keluargas');
        $dataPenduduk = DB::table('penduduks')->rightJoin('kartu_keluargas', 'penduduks.id_kk', 'kartu_keluargas.id');
        $dataKelahiran = DB::table('kelahirans')->join('kartu_keluargas', 'kelahirans.id_kk', 'kartu_keluargas.id');
        $dataKematian = DB::table('kematians')->join('penduduks', 'kematians.id_ktp', 'penduduks.id')->join('kartu_keluargas', 'kartu_keluargas.id', 'kematians.id_kk');
        $dataPindahDatang = DB::table('pindahs')->where('jenis', 'datang');
        $dataPindahPergi = DB::table('pindahs')->where('jenis', 'pergi');
        $from = date("Y-m-d 00:00:00", strtotime($request->from));
        $to = date("Y-m-d 23:59:59", strtotime($request->to));
        $REKAP= array();

        // Conditions
        if($request->from != null){
            $kartuKeluarga = $kartuKeluarga->where('created_at', '>', $from);
            $dataPenduduk = $dataPenduduk->where('penduduks.created_at', '>', $from);
            $dataKelahiran = $dataKelahiran->where('kelahirans.created_at', '>', $from);
            $dataKematian = $dataKematian->where('kematians.created_at', '>', $from);
            $dataPindahDatang = $dataPindahDatang->where('created_at', '>', $from);
            $dataPindahPergi = $dataPindahPergi->where('created_at', '>', $from);       
        }
        if($request->to != null){
            $kartuKeluarga = $kartuKeluarga->where('created_at', '<', $to);
            $dataPenduduk = $dataPenduduk->where('penduduks.created_at', '<', $to);
            $dataKelahiran = $dataKelahiran->where('kelahirans.created_at', '<', $to);
            $dataKematian = $dataKematian->where('kematians.created_at', '<', $to);
            $dataPindahDatang = $dataPindahDatang->where('created_at', '<', $to);
            $dataPindahPergi = $dataPindahPergi->where('created_at', '<', $to);          
        }
        if($request->type == 'Jenis Kelamin'){
            $kartuKeluarga = $kartuKeluarga->get();
            $dataPenduduk = $dataPenduduk->get();
            $dataKelahiran = $dataKelahiran->get();
            $dataKematian = $dataKematian->get();
            $dataPindahDatang = $dataPindahDatang->get();
            $dataPindahPergi = $dataPindahPergi->get();

            foreach($kelurahan as $data){
                // Kartu Keluarga
                $REKAP[$data->kelurahan]['KK'] = 0;

                // Data Penduduk
                $REKAP[$data->kelurahan]['DP']['L'] = 0;
                $REKAP[$data->kelurahan]['DP']['P'] = 0;

                // Data Kelahiran
                $REKAP[$data->kelurahan]['KLR']['L'] = 0;
                $REKAP[$data->kelurahan]['KLR']['P'] = 0;

                // Data Kematian
                $REKAP[$data->kelurahan]['KMT']['L'] = 0;
                $REKAP[$data->kelurahan]['KMT']['P'] = 0;

                // Data Pindah Datang
                $REKAP[$data->kelurahan]['DTG']['L'] = 0;
                $REKAP[$data->kelurahan]['DTG']['P'] = 0;

                // Data Pindah Pergi
                $REKAP[$data->kelurahan]['PRG']['L'] = 0;
                $REKAP[$data->kelurahan]['PRG']['P'] = 0;
            }
            foreach($kartuKeluarga as $data){
                $REKAP[$data->kelurahan]['KK']++;
            }
            foreach($dataPenduduk as $data){
                if($data->jenis_kelamin == "Laki - Laki")
                    $REKAP[$data->kelurahan]['DP']['L']++;
                if($data->jenis_kelamin == "Perempuan")
                    $REKAP[$data->kelurahan]['DP']['P']++;
            }
            foreach($dataKelahiran as $data){
                if($data->jenis_kelamin == "Laki - Laki")
                    $REKAP[$data->kelurahan]['KLR']['L']++;
                if($data->jenis_kelamin == "Perempuan")
                    $REKAP[$data->kelurahan]['KLR']['P']++;
            }
            foreach($dataKematian as $data){
                if($data->jenis_kelamin == "Laki - Laki")
                    $REKAP[$data->kelurahan]['KMT']['L']++;
                if($data->jenis_kelamin == "Perempuan")
                    $REKAP[$data->kelurahan]['KMT']['P']++;
            }
            foreach($dataPindahDatang as $data){
                if($data->jenis_kelamin == "Laki - Laki")
                    $REKAP[$data->kelurahan]['DTG']['L']++;
                if($data->jenis_kelamin == "Perempuan")
                    $REKAP[$data->kelurahan]['DTG']['P']++;
            }
            foreach($dataPindahPergi as $data){
                if($data->jenis_kelamin == "Laki - Laki")
                    $REKAP[$data->kelurahan]['PRG']['L']++;
                if($data->jenis_kelamin == "Perempuan")
                    $REKAP[$data->kelurahan]['PRG']['P']++;
            }

        }

        if($request->type == 'Agama')
        {
            $kartuKeluarga = $kartuKeluarga->get();
            $dataPenduduk = $dataPenduduk->get();
            $dataKelahiran = $dataKelahiran->get();
            $dataKematian = $dataKematian->get();
            $dataPindahDatang = $dataPindahDatang->get();
            $dataPindahPergi = $dataPindahPergi->get();

            foreach($kelurahan as $data){
                // Kartu Keluarga                
                $REKAP[$data->kelurahan]['KK'] = 0; 

                // Data Penduduk
                $REKAP[$data->kelurahan]['DP']['1'] = 0;
                $REKAP[$data->kelurahan]['DP']['2'] = 0;
                $REKAP[$data->kelurahan]['DP']['3'] = 0;
                $REKAP[$data->kelurahan]['DP']['4'] = 0;
                $REKAP[$data->kelurahan]['DP']['5'] = 0;
                $REKAP[$data->kelurahan]['DP']['6'] = 0;

                // Data Kelahiran
                $REKAP[$data->kelurahan]['KLR']['1'] = 0;
                $REKAP[$data->kelurahan]['KLR']['2'] = 0;
                $REKAP[$data->kelurahan]['KLR']['3'] = 0;
                $REKAP[$data->kelurahan]['KLR']['4'] = 0;
                $REKAP[$data->kelurahan]['KLR']['5'] = 0;
                $REKAP[$data->kelurahan]['KLR']['6'] = 0;

                // Data Kematian
                $REKAP[$data->kelurahan]['KMT']['1'] = 0;
                $REKAP[$data->kelurahan]['KMT']['2'] = 0;
                $REKAP[$data->kelurahan]['KMT']['3'] = 0;
                $REKAP[$data->kelurahan]['KMT']['4'] = 0;
                $REKAP[$data->kelurahan]['KMT']['5'] = 0;
                $REKAP[$data->kelurahan]['KMT']['6'] = 0;

                // Data Pindah Datang
                $REKAP[$data->kelurahan]['DTG']['1'] = 0;
                $REKAP[$data->kelurahan]['DTG']['2'] = 0;
                $REKAP[$data->kelurahan]['DTG']['3'] = 0;
                $REKAP[$data->kelurahan]['DTG']['4'] = 0;
                $REKAP[$data->kelurahan]['DTG']['5'] = 0;
                $REKAP[$data->kelurahan]['DTG']['6'] = 0;

                // Data Pindah Pergi
                $REKAP[$data->kelurahan]['PRG']['1'] = 0;
                $REKAP[$data->kelurahan]['PRG']['2'] = 0;
                $REKAP[$data->kelurahan]['PRG']['3'] = 0;
                $REKAP[$data->kelurahan]['PRG']['4'] = 0;
                $REKAP[$data->kelurahan]['PRG']['5'] = 0;
                $REKAP[$data->kelurahan]['PRG']['6'] = 0;
            }

            foreach($kartuKeluarga as $data){
                $REKAP[$data->kelurahan]['KK']++;
            }
            foreach($dataPenduduk as $data){
                if($data->agama == "Islam")
                    $REKAP[$data->kelurahan]['DP']['1']++;
                if($data->agama == "Kristen")
                    $REKAP[$data->kelurahan]['DP']['2']++;
                if($data->agama == "Katolik")
                    $REKAP[$data->kelurahan]['DP']['3']++;
                if($data->agama == "Hindu")
                    $REKAP[$data->kelurahan]['DP']['4']++;
                if($data->agama == "Budha")
                    $REKAP[$data->kelurahan]['DP']['5']++;
                if($data->agama == "Lainnya")
                    $REKAP[$data->kelurahan]['DP']['6']++;
            }
            foreach($dataKelahiran as $data){
                if($data->agama == "Islam")
                    $REKAP[$data->kelurahan]['KLR']['1']++;
                if($data->agama == "Kristen")
                    $REKAP[$data->kelurahan]['KLR']['2']++;
                if($data->agama == "Katolik")
                    $REKAP[$data->kelurahan]['KLR']['3']++;
                if($data->agama == "Hindu")
                    $REKAP[$data->kelurahan]['KLR']['4']++;
                if($data->agama == "Budha")
                    $REKAP[$data->kelurahan]['KLR']['5']++;
                if($data->agama == "Lainnya")
                    $REKAP[$data->kelurahan]['KLR']['6']++;
            }
            foreach($dataKematian as $data){
                if($data->agama == "Islam")
                    $REKAP[$data->kelurahan]['KMT']['1']++;
                if($data->agama == "Kristen")
                    $REKAP[$data->kelurahan]['KMT']['2']++;
                if($data->agama == "Katolik")
                    $REKAP[$data->kelurahan]['KMT']['3']++;
                if($data->agama == "Hindu")
                    $REKAP[$data->kelurahan]['KMT']['4']++;
                if($data->agama == "Budha")
                    $REKAP[$data->kelurahan]['KMT']['5']++;
                if($data->agama == "Lainnya")
                    $REKAP[$data->kelurahan]['KMT']['6']++;
            }
            foreach($dataPindahDatang as $data){
                if($data->agama == "Islam")
                    $REKAP[$data->kelurahan]['DTG']['1']++;
                if($data->agama == "Kristen")
                    $REKAP[$data->kelurahan]['DTG']['2']++;
                if($data->agama == "Katolik")
                    $REKAP[$data->kelurahan]['DTG']['3']++;
                if($data->agama == "Hindu")
                    $REKAP[$data->kelurahan]['DTG']['4']++;
                if($data->agama == "Budha")
                    $REKAP[$data->kelurahan]['DTG']['5']++;
                if($data->agama == "Lainnya")
                    $REKAP[$data->kelurahan]['DTG']['6']++;
            }
            foreach($dataPindahPergi as $data){
                if($data->agama == "Islam")
                    $REKAP[$data->kelurahan]['PRG']['1']++;
                if($data->agama == "Kristen")
                    $REKAP[$data->kelurahan]['PRG']['2']++;
                if($data->agama == "Katolik")
                    $REKAP[$data->kelurahan]['PRG']['3']++;
                if($data->agama == "Hindu")
                    $REKAP[$data->kelurahan]['PRG']['4']++;
                if($data->agama == "Budha")
                    $REKAP[$data->kelurahan]['PRG']['5']++;
                if($data->agama == "Lainnya")
                    $REKAP[$data->kelurahan]['PRG']['6']++;
            }
        }
        
        if($request->type == 'Kewarganegaraan'){
            $kartuKeluarga = $kartuKeluarga->get();
            $dataPenduduk = $dataPenduduk->get();
            $dataKelahiran = $dataKelahiran->get();
            $dataKematian = $dataKematian->get();
            $dataPindahDatang = $dataPindahDatang->get();
            $dataPindahPergi = $dataPindahPergi->get();

            foreach($kelurahan as $data){
                $REKAP[$data->kelurahan]['KK'] = 0;
                $REKAP[$data->kelurahan]['DP']['WNI'] = 0;
                $REKAP[$data->kelurahan]['DP']['WNA'] = 0;
                $REKAP[$data->kelurahan]['KLR']['WNI'] = 0;
                $REKAP[$data->kelurahan]['KLR']['WNA'] = 0;
                $REKAP[$data->kelurahan]['KMT']['WNI'] = 0;
                $REKAP[$data->kelurahan]['KMT']['WNA'] = 0;
                $REKAP[$data->kelurahan]['DTG']['WNI'] = 0;
                $REKAP[$data->kelurahan]['DTG']['WNA'] = 0;
                $REKAP[$data->kelurahan]['PRG']['WNI'] = 0;
                $REKAP[$data->kelurahan]['PRG']['WNA'] = 0;
            }
            foreach($kartuKeluarga as $data){
                $REKAP[$data->kelurahan]['KK']++;
            }
            foreach($dataPenduduk as $data){
                if($data->kewarganegaraan == "WNI")
                    $REKAP[$data->kelurahan]['DP']['WNI']++;
                if($data->kewarganegaraan == "WNA")
                    $REKAP[$data->kelurahan]['DP']['WNA']++;
            }
            foreach($dataKelahiran as $data){
                if($data->kewarganegaraan == "WNI")
                    $REKAP[$data->kelurahan]['KLR']['WNI']++;
                if($data->kewarganegaraan == "WNA")
                    $REKAP[$data->kelurahan]['KLR']['WNA']++;
            }
            foreach($dataKematian as $data){
                if($data->kewarganegaraan == "WNI")
                    $REKAP[$data->kelurahan]['KMT']['WNI']++;
                if($data->kewarganegaraan == "WNA")
                    $REKAP[$data->kelurahan]['KMT']['WNA']++;
            }
            foreach($dataPindahDatang as $data){
                if($data->kewarganegaraan == "WNI")
                    $REKAP[$data->kelurahan]['DTG']['WNI']++;
                if($data->kewarganegaraan == "WNA")
                    $REKAP[$data->kelurahan]['DTG']['WNA']++;
            }
            foreach($dataPindahPergi as $data){
                if($data->kewarganegaraan == "WNI")
                    $REKAP[$data->kelurahan]['PRG']['WNI']++;
                if($data->kewarganegaraan == "WNA")
                    $REKAP[$data->kelurahan]['PRG']['WNA']++;
            }
        }

        return view('admin.rekap-print')
            ->with('typeLaporan', 'Rekapitulasi Data')
            ->with('filter', $request)
            ->with('count', null)
            ->with('q', null)
            ->with('REKAP', $REKAP);
    }
}
