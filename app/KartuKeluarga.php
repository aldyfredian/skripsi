<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class KartuKeluarga extends Model
{
	protected $table = 'kartu_keluargas';

	public function getKKs($queriesArray, $limit, $offset){
		$tempQuery = DB::table($this->table);
		foreach($queriesArray as $key => $query){
            //build search queries
            if($query != ''){
	            if($key=='no_kk') {
					$tempQuery->where('no_kk', 'like', '%'.$query.'%');
				}
				else if($key=='kepala_keluarga') {
					$tempQuery->where('kepala_keluarga', 'like', '%'.$query.'%');
				} 
				else if($key=='alamat') {
					$tempQuery->where('alamat', 'like', '%'.$query.'%');
				} 
				else if($key=='rt') {
					$tempQuery->where('rt', 'like', '%'.$query.'%');
				} 
				else if($key=='rw') {
					$tempQuery->where('rw', 'like', '%'.$query.'%');
				} 
				else if($key=='kelurahan') {
					$tempQuery->where('kelurahan', 'like', '%'.$query.'%');
				} 
				else if($key=='kode_pos') {
					$tempQuery->where('kode_pos', 'like', '%'.$query.'%');
				}
			}
        }
        
        $data['sql'] = $tempQuery->toSql();
        $data['count'] = $tempQuery->count();
        $data['result'] = $tempQuery->limit($limit)->offset($offset)->get();
        return $data;
    }
}
