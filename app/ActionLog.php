<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActionLog extends Model
{
    protected $fillable = [
			'id_user',
			'action'
	];


	public $timesteamps = false;

	protected $table = 'action_logs';
}
