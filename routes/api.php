<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// API User
Route::get('get_datatable_user','UserController@getDatatableUsers');
Route::post('get_datatable_user','UserController@getDatatableUsers');

// API Kartu Keluarga
Route::get('get_kks','KartuKeluargaController@getKKs');
Route::get('get_datatable_kk','KartuKeluargaController@getDatatableKKs');
Route::post('get_datatable_kk','KartuKeluargaController@getDatatableKKs');

// API Pekerjaan
Route::get('get_pekerjaans','PekerjaanController@getPekerjaan');
Route::get('get_datatable_pekerjaan','PekerjaanController@getDatatablePekerjaans');
Route::post('get_datatable_pekerjaan','PekerjaanController@getDatatablePekerjaans');

// API Penduduk
Route::get('get_penduduk','PendudukController@getPenduduks');
Route::get('get_individu','PendudukController@getIndividu');
Route::get('get_datatable_penduduk','PendudukController@getDatatablePenduduks');
Route::post('get_datatable_penduduk','PendudukController@getDatatablePenduduks');

// API Kelahiran
Route::get('get_datatable_kelahiran','KelahiranController@getDatatableKelahirans');
Route::post('get_datatable_kelahiran','KelahiranController@getDatatableKelahirans');

// API Kematian
Route::get('get_datatable_kematian','KematianController@getDatatableKematians');
Route::post('get_datatable_kematian','KematianController@getDatatableKematians');

// API Pindah Datang
Route::get('get_datatable_pindah_datang','PindahDatangController@getDatatableDatangs');
Route::post('get_datatable_pindah_datang','PindahDatangController@getDatatableDatangs');

// API Pindah Pergi
Route::get('get_datatable_pindah_pergi','PindahPergiController@getDatatablePergis');
Route::post('get_datatable_pindah_pergi','PindahPergiController@getDatatablePergis');

// API Action Logs
Route::get('get_datatable_action_log','ActionLogController@getDatatableLogs');
Route::post('get_datatable_action_log','ActionLogController@getDatatableLogs');