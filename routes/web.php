<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function(){	
	Route::resource('/', 'DashboardController');
	Route::post('admin/kelahiran/print', 'KelahiranController@print');
	Route::post('admin/kematian/print', 'KematianController@print');
	Route::post('admin/pindah/datang/print', 'PindahDatangController@print');
	Route::post('admin/pindah/pergi/print', 'PindahPergiController@print');
	Route::post('admin/master/kartu_keluarga/print', 'KartuKeluargaController@print');
	Route::post('admin/master/data_penduduk/print', 'PendudukController@print');
});

// Admin
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

	// Dashboard
	Route::resource('/', 'DashboardController');

	// User Management
	Route::resource('user_management', 'UserController');
	Route::get('change_password', 'UserController@pass');
	Route::put('change_password', 'UserController@updatePass');

	// Master Data
	Route::group(['prefix' => 'master'], function () {

		// Kartu Keluarga
		Route::resource('kartu_keluarga', 'KartuKeluargaController');

		// Data Penduduk
		Route::resource('data_penduduk', 'PendudukController');

		// Jenis Pekerjaan
		Route::resource('jenis_pekerjaan', 'PekerjaanController');
	});

	// Data Kelahiran
	Route::resource('kelahiran', 'KelahiranController');	
	Route::get('kelahiran/{id}/process_document', 'KelahiranController@processDoc');
	Route::get('kelahiran/{id}/pending_document', 'KelahiranController@pendingDoc');
	Route::get('kelahiran/{id}/finish_document', 'KelahiranController@finishDoc');

	// Data Kematian
	Route::resource('kematian', 'KematianController');	
	Route::get('kematian/{id}/process_document', 'KematianController@processDoc');
	Route::get('kematian/{id}/pending_document', 'KematianController@pendingDoc');	
	Route::get('kematian/{id}/finish_document', 'KematianController@finishDoc');

	// Data Pindah
	Route::group(['prefix' => 'pindah'], function () {
		// Data Pindah Datang
		Route::resource('datang', 'PindahDatangController');
		Route::get('datang/{id}/process_document', 'PindahDatangController@processDoc');
		Route::get('datang/{id}/pending_document', 'PindahDatangController@pendingDoc');	
		Route::get('datang/{id}/finish_document', 'PindahDatangController@finishDoc');

		// Data Pindah Pergi
		Route::resource('pergi', 'PindahPergiController');
		Route::get('pergi/{id}/process_document', 'PindahPergiController@processDoc');
		Route::get('pergi/{id}/pending_document', 'PindahPergiController@pendingDoc');	
		Route::get('pergi/{id}/finish_document', 'PindahPergiController@finishDoc');
	});

	// Action Logs / Catatan Aksi
	Route::resource('action_logs', 'ActionLogController');

	// Report / Laporan
	Route::get('rekap', 'RekapController@index');
	Route::post('rekap/print', 'RekapController@print');
});

Auth::routes();
Route::get('logout', 'Auth\Logincontroller@logout');
Route::post('logout', 'Auth\LoginController@logout');