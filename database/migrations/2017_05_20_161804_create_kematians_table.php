<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKematiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kematians', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_skk');
            $table->unsignedInteger('id_ktp');
            $table->unsignedInteger('id_kk');
            $table->dateTime('waktu_kematian');
            $table->string('status');
            $table->timestamps();

            $table->foreign('id_kk')->references('id')->on('kartu_keluargas');
            $table->foreign('id_ktp')->references('id')->on('penduduks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kematians');
    }
}
