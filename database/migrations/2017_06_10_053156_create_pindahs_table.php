<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePindahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pindahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_registrasi');            
            $table->string('nik');            
            $table->string('no_kk');            
            $table->string('nama');            
            $table->string('kota_asal');            
            $table->string('kecamatan_asal');            
            $table->string('kota_tujuan');            
            $table->string('kecamatan_tujuan');            
            $table->string('alamat_lama');            
            $table->string('alamat_baru');            
            $table->string('jenis');            
            $table->string('agama');            
            $table->string('kewarganegaraan');            
            $table->string('kelurahan');            
            $table->string('jenis_kelamin');  
            $table->string('status');          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pindahs');
    }
}
