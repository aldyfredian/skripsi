<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKelahiransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kelahirans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_skk');
            $table->string('nama');
            $table->dateTime('waktu_kelahiran');
            $table->string('tempat_lahir');
            $table->string('agama');
            $table->string('jenis_kelamin');
            $table->string('kewarganegaraan');
            $table->string('nama_ibu');
            $table->string('nama_ayah')->nullable();
            $table->string('tempat_nikah');
            $table->date('tanggal_nikah');
            $table->string('no_buku_nikah');
            $table->string('status');
            $table->timestamps();

            $table->unsignedInteger('id_kk');
            $table->foreign('id_kk')->references('id')->on('kartu_keluargas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kelahirans');
    }
}
