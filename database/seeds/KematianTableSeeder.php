<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class KematianTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_kk = DB::table('kartu_keluargas')->pluck('id')->all();
        $id_ktp = DB::table('penduduks')->pluck('id')->all();
        
        $faker = Faker::create();
        for ($i=1; $i <= 1000; $i++){
        	$date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
        	$death = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');

        	$data[] = [
                'no_skk'			=> $faker->unique()->numberBetween($min = 32739040001, $max = 32739062000),                
        		'id_kk'				=> $faker->randomElement($id_kk),
        		'id_ktp'			=> $faker->randomElement($id_ktp),
        		'waktu_kematian'	=> $death,
        		'created_at'		=> $date, 
        		'updated_at'		=> $date,
                'status'             => $faker->randomElement(['0', '1', '2'])
        	];
        }
        DB::table('kematians')->insert($data);
    }
}
