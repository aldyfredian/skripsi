<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class KartuKeluargaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jalan = ['Balonggede', 'Ancol', 'Denki', 'Sriwijaya', 'Kembar', 'Kembar Dalam', 'Kembar I', 'Kembar II', 'Kembar IV', 'BKR', 'Pasir Salam', 'Pasirluyu', 'Srimahi', 'Mengger Girang', 'Ciburuy', 'Srisuci', 'Sriayu', 'Srielok', 'H. Samsudin', 'Pungkur', 'Moch. Ramdan', 'Ibu Inggit Garnasih'];

        $dpL = ['Asep', 'Rahmat', 'Udin', 'Rangga', 'Muhammad', 'Cecep', 'Maman', 'Rusli', 'Airlangga', 'Aldy', 'Ramdhani', 'Imam', 'Iman', 'Kusnadi', 'Ari', 'Andi', 'Kuncoro', 'Jajang', 'Yanto', 'Iyus', 'Rusman', 'Harry', 'Tubagus', 'Adri', 'Deni', 'Dany', 'Aditya', 'Haryanto', 'Iwan', 'Satrio', 'Dito', 'Angga', 'Dion', 'Kenzie', 'Irfan', 'Marselino', 'Dadang', 'Eko', 'Yoga', 'Yogi', 'Reza', 'Jody', 'Rizki', 'Selamet', 'Riyadi'];

        $bkL = ['Saepudin', 'Nurjaman', 'Ismail', 'Sutanto', 'Suratno', 'Nugraha', 'Nurjaman', 'Jaka', 'Rismanto', 'Utoro', 'Hartono', 'Priatna', 'Surasep', 'Tri Dharma', 'Yulianto', 'Fauzie', 'Eka', 'Wicaksono', 'Arief', 'Purwanto', 'Kustiawan'];

        $dpP = ['Desma', 'Risma', 'Marissa', 'Anissa', 'Nurul', 'Maya', 'Siti', 'Novi', 'Lia', 'Mia', 'Nia', 'Kayra', 'Nadia', 'Nadya', 'Lily'];
        $bkP = ['Amalia', 'Putri', 'Eka', 'Ismail', 'Anastasia', 'Fajriati', 'Nurawalia', 'Ulfah', 'Pertiwi', 'Indah', 'Regina', 'Silviana', 'Agustina'];

        $faker = Faker::create();
        for ($i=1; $i <= 800; $i++){
            $date = $faker->dateTimeBetween($startDate = '-700 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_kk'              => $faker->unique()->numberBetween($min = 32730101010001, $max = 32730101012001),
                'kepala_keluarga'    => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'alamat'             => 'Jl. '.$faker->randomElement($jalan).' No. '.$faker->numberBetween($min = 1, $max = 87),
                'rt'                 => $faker->randomDigitNotNull,
                'rw'                 => $faker->randomDigitNotNull,
                'kelurahan'          => $faker->randomElement($array = ['Ancol', 'Balonggede', 'Ciateul', 'Ciseureuh', 'Cigereleng', 'Pasirluyu', 'Pungkur']),
                'created_at'         => $date, 
                'updated_at'         => $date
            ];
        }
        for ($i=1; $i <= 200; $i++){
            $date = $faker->dateTimeBetween($startDate = '-700 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $data[] = [
                'no_kk'              => $faker->unique()->numberBetween($min = 32730101012002, $max = 32730101017001),
                'kepala_keluarga'    => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'alamat'             => 'Jl. '.$faker->randomElement($jalan).' No. '.$faker->numberBetween($min = 1, $max = 87),
                'rt'                 => $faker->randomDigitNotNull,
                'rw'                 => $faker->randomDigitNotNull,
                'kelurahan'          => $faker->randomElement($array = ['Ancol', 'Balonggede', 'Ciateul', 'Ciseureuh', 'Cigereleng', 'Pasirluyu', 'Pungkur']),
                'created_at'         => $date, 
                'updated_at'         => $date
            ];
        }
        DB::table('kartu_keluargas')->insert($data);
    }
}
