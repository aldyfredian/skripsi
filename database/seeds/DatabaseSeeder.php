<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        //$this->call(KartuKeluargaTableSeeder::class);
        //$this->call(PekerjaanTableSeeder::class);
        $this->call(PendudukTableSeeder::class);
        //$this->call(KelahiranTableSeeder::class);
        //$this->call(KematianTableSeeder::class);
        //$this->call(PindahTableSeeder::class);
    }
}
