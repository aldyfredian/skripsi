<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class KelahiranTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id_kk = DB::table('kartu_keluargas')->pluck('id')->all();
        
        $dpL = ['Asep', 'Rahmat', 'Udin', 'Rangga', 'Muhammad', 'Cecep', 'Maman', 'Rusli', 'Airlangga', 'Aldy', 'Ramdhani', 'Imam', 'Iman', 'Kusnadi', 'Ari', 'Andi', 'Kuncoro', 'Jajang', 'Yanto', 'Iyus', 'Rusman', 'Harry', 'Tubagus'];
        $bkL = ['Saepudin', 'Nurjaman', 'Ismail', 'Sutanto', 'Suratno', 'Nugraha', 'Nurjaman', 'Jaka', 'Rismanto', 'Utoro', 'Hartono', 'Priatna', 'Surasep', 'Tri Dharma', 'Yulianto', 'Fauzie', 'Eka', 'Wicaksono', 'Arief', 'Purwanto', 'Kustiawan'];

        $dpLA = ['Michael', 'Steven', 'Marshall', 'Theodore', 'Barnabus', 'Shannon', 'Oliver', 'Christian', 'Marcell', 'Maximillian', 'Brillian', 'Jonathan', 'John', 'Joseph'];
        $bkLA = ['Dickson', 'Oracle', 'Ericksen', 'Aldrin', 'Armstrong', 'Stinson', 'Johnson', 'William', 'Mosby', 'Cooper', 'Hoftsader'];

        $dpP = ['Desma', 'Risma', 'Marissa', 'Anissa', 'Nurul', 'Maya', 'Siti', 'Novi', 'Lia', 'Mia', 'Nia', 'Kayra', 'Nadia', 'Nadya', 'Lily'];
        $bkP = ['Amalia', 'Putri', 'Eka', 'Ismail', 'Anastasia', 'Fajriati', 'Nurawalia', 'Ulfah', 'Pertiwi', 'Indah', 'Regina', 'Silviana', 'Agustina'];

        $dpPA = ['Loretta', 'Jenny', 'Mia', 'Sasha', 'Penny', 'Amy', 'Rihanna', 'Brie', 'Nicole'];
        $bkPA = ['Stinson', 'White', 'Blighe', 'Farah', 'Fowler', 'Bella', 'Kidman', 'Witherspoon', 'Jasmine'];

        $cI = ['Bandung', 'Jakarta', 'Bekasi', 'Tanggerang', 'Depok', 'Bogor', 'Surabaya', 'Denpasar'];

        $faker = Faker::create();

        for ($i=1; $i <= 600; $i++){
        	$date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
        	$born = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
        	$married = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d');
        	$data[] = [
                'no_skk'              => $faker->unique()->numberBetween($min = 32739040001, $max = 32739061000),
                'nama'	 			  => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'waktu_kelahiran'    => $born,
                'tempat_lahir'		 => $faker->randomElement($cI),
                'agama'				 =>	$faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'jenis_kelamin'		 =>'Laki - Laki',
                'kewarganegaraan'	 => 'WNI',
                'nama_ibu'			 => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'nama_ayah'			 => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'tanggal_nikah'		 => $married,
                'no_buku_nikah'		 => $faker->unique()->numberBetween($min = 709390001, $max = 709390701),
                'tempat_nikah'       => $faker->randomElement($cI),
        		'created_at' 		 => $date, 
        		'updated_at' 		 => $date,
        		'id_kk'				 => $faker->randomElement($id_kk),
                'status'             => $faker->randomElement(['0', '1', '2'])
        	];
        }

        for ($i=1; $i <= 600; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $married = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d');
            $data[] = [
                'no_skk'              => $faker->unique()->numberBetween($min = 32739041001, $max = 32739062000),
                'nama'                => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'waktu_kelahiran'    => $born,
                'tempat_lahir'       => $faker->randomElement($cI),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'jenis_kelamin'      =>'Perempuan',
                'kewarganegaraan'    => 'WNI',
                'nama_ibu'           => $faker->randomElement($dpP).' '.$faker->randomElement($bkP),
                'nama_ayah'          => $faker->randomElement($dpL).' '.$faker->randomElement($bkL),
                'tanggal_nikah'      => $married,
                'no_buku_nikah'      => $faker->unique()->numberBetween($min = 709390702, $max = 709391401),
                'tempat_nikah'       => $faker->randomElement($cI),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk),
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 150; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $married = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d');
            $data[] = [
                'no_skk'              => $faker->unique()->numberBetween($min = 32739042001, $max = 32739063000),
                'nama'                => $faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'waktu_kelahiran'    => $born,
                'tempat_lahir'       => $faker->randomElement($cI),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'jenis_kelamin'      => 'Laki - Laki',
                'kewarganegaraan'    => 'WNA',
                'nama_ibu'           => $faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'nama_ayah'          => $faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'tanggal_nikah'      => $married,
                'no_buku_nikah'      => $faker->unique()->numberBetween($min = 709391402, $max = 709391701),
                'tempat_nikah'       => $faker->randomElement($cI),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk),
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }

        for ($i=1; $i <= 150; $i++){
            $date = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $born = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d H:i:s');
            $married = $faker->dateTimeBetween($startDate = '-1000 days', $endDate = 'now')->format('Y-m-d');
            $data[] = [
                'no_skk'              => $faker->unique()->numberBetween($min = 32739030001, $max = 32739064000),
                'nama'                => $faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'waktu_kelahiran'    => $born,
                'tempat_lahir'       => $faker->randomElement($cI),
                'agama'              => $faker->randomElement($array = ['Islam', 'Kristen', 'Katolik', 'Hindu', 'Budha', 'Lainnya']),
                'jenis_kelamin'      => 'Perempuan',
                'kewarganegaraan'    => 'WNA',
                'nama_ibu'           => $faker->randomElement($dpPA).' '.$faker->randomElement($bkPA),
                'nama_ayah'          => $faker->randomElement($dpLA).' '.$faker->randomElement($bkLA),
                'tanggal_nikah'      => $married,
                'no_buku_nikah'      => $faker->unique()->numberBetween($min = 709391702, $max = 709392001),
                'tempat_nikah'       => $faker->randomElement($cI),
                'created_at'         => $date, 
                'updated_at'         => $date,
                'id_kk'              => $faker->randomElement($id_kk),
                'status'             => $faker->randomElement(['0', '1', '2'])
            ];
        }
        DB::table('kelahirans')->insert($data);
    }
}
