@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Pekerjaan
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li><a class="link-effect" href="{{URL::to('admin/master/jenis_pekerjaan')}}">Pekerjaan</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a href="{{URL::to('admin/master/jenis_pekerjaan/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table id="dataTable" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="100px">Kode</th>
                                        <th>Nama Pekerjaan</th>
                                        <th width="150px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open(['method' => 'delete','route' => ['jenis_pekerjaan.destroy', null],'class' => 'hiddenDeleteForm'])!!}
    {!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function() {
        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "kode" },
                { "data": "nama" },
                { "data": "action" },
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('PekerjaanController@getDatatablePekerjaans') }}',
              "type": "POST",
              "data": {
                  @foreach(app('request')->input() as $key => $query)
                    @if ($query!='')
                      "{{ $key }}": "{{ $query }}",
                    @endif
                  @endforeach
              }
            },
            deferRender: true
        });

        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('PekerjaanController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@endsection