@extends('admin.layouts.app')

@section('content')
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/master/jenis_pekerjaan')}}">Jenis Pekerjaan</a>
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Jenis Pekerjaan</h3>
                                </div>
                                <div class="block-content">
                                @if($action=='jenis_pekerjaan.store')
                                  {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                @else
                                  {!! Form::open(['route' => [$action, $pekerjaan->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                @endif
                                        <div class="form-group">
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    <input value="{{ $pekerjaan->kode }}" class="form-control" type="text" id="kode" name="kode">
                                                    <label for="kode">Kode Pekerjaan</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="form-material floating">
                                                    <input value="{{ $pekerjaan->nama }}" class="form-control" type="text" id="nama" name="nama">
                                                    <label for="nama">Nama Pekerjaan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-plus push-5-r"></i> Register Data</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
@endsection