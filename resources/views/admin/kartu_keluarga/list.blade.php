@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Kartu Keluarga
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li><a class="link-effect" href="{{URL::to('admin/master/kartu_keluarga')}}">Kartu Keluarga</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a data-toggle="modal" data-target="#modal-popin" type="button" class="btn btn-info btn-rounded">
                                <i class="fa fa-filter"></i> Filter Data
                            </a>
                            @if(Auth::user()->role == 'Admin')
                            <a id="print-btn" type="button" class="btn btn-success btn-rounded">
                                <i class="fa fa-print"></i> Print Data
                            </a>
                            @endif
                            <a href="{{URL::to('admin/master/kartu_keluarga/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>No. KK</th>
                                        <th>Kepala Keluarga</th>
                                        <th>Alamat</th>
                                        <th>Kelurahan</th>
                                        <th width="150px">Aksi</th>
                                    </tr>
                                </thead>

                                {{-- Data Loaded Through AJAX --}}
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open([
            'method' => 'delete', 
            'route' => ['kartu_keluarga.destroy', null],
            'class' => 'hiddenDeleteForm'
    ])!!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('start', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-awal']) !!}
                                    <label>Tanggal Registrasi</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('end', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-akhir']) !!}
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">                                    
                                    {!! Form::text('nokk', null, ['class' => 'form-control', 'id' => 'filter-no-kk']) !!}
                                    <label>No. Kartu Keluarga</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'id' => 'filter-kepala-keluarga']) !!}
                                    <label>Nama Kepala Keluarga</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-alamat']) !!}
                                    <label>Alamat</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-rt']) !!}
                                    <label>RT</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-rw']) !!}
                                    <label>RW</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        'Ancol' => 'Ancol',
                                        'Balonggede' => 'Balonggede',
                                        'Ciateul' => 'Ciateul',
                                        'Cigereleng' => 'Cigereleng',
                                        'Ciseureuh' => 'Ciseureuh',
                                        'Pasirluyu' => 'Pasirluyu',
                                        'Pungkur' => 'Pungkur'], null, ['class' => 'form-control', 'placeholder' => '', 'id' => 'filter-kelurahan']) !!}
                                    <label>Kelurahan</label>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->
    {!! Form::open(['action' => 'KartuKeluargaController@print', 'id' => 'printForm', 'method' => 'POST']) !!}
        {!! Form::hidden('nama', null, ['id' => 'print-nama']) !!}
        {!! Form::hidden('nokk', null, ['id' => 'print-no-kk']) !!}
        {!! Form::hidden('tglAwal', null, ['id' => 'print-tanggal-awal']) !!}
        {!! Form::hidden('tglAkhir', null, ['id' => 'print-tanggal-akhir']) !!}
        {!! Form::hidden('kelurahan', null, ['id' => 'print-kelurahan']) !!}
        {!! Form::hidden('alamat', null, ['id' => 'print-alamat']) !!}
        {!! Form::hidden('rt', null, ['id' => 'print-rt']) !!}
        {!! Form::hidden('rw', null, ['id' => 'print-rw']) !!}
    {!! Form::close() !!}

  <script type="text/javascript">
    $(document).ready(function() {
        function buildhidden() {
            document.getElementById("print-nama").value = $('#filter-kepala-keluarga').val();
            document.getElementById("print-no-kk").value = $('#filter-no-kk').val();
            document.getElementById("print-tanggal-awal").value = $('#filter-tgl-awal').val();
            document.getElementById("print-tanggal-akhir").value = $('#filter-tgl-akhir').val();
            document.getElementById("print-kelurahan").value = $('#filter-kelurahan').val();
            document.getElementById("print-alamat").value = $('#filter-alamat').val();
            document.getElementById("print-rt").value = $('#filter-rt').val();
            document.getElementById("print-rw").value = $('#filter-rw').val();
        }

        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
        });

        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "no_kk" },
                { "data": "kepala_keluarga" },
                { "data": "alamat" },
                { "data": "kelurahan" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('KartuKeluargaController@getDatatableKKs') }}',
              "type": "POST",
              "data":  function(d) {
                   d.nokk = $('#filter-no-kk').val(),
                   d.nama = $('#filter-kepala-keluarga').val(),
                   d.tglAwal = $('#filter-tgl-awal').val(),
                   d.tglAkhir = $('#filter-tgl-akhir').val(),
                   d.alamat = $('#filter-alamat').val(),
                   d.rt = $('#filter-rt').val(),
                   d.rw = $('#filter-rw').val(),
                   d.kelurahan = $('#filter-kelurahan').val()
              }
            },
            deferRender: true
        });

        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            buildhidden();
            t.draw();
        });

        $('#print-btn').click(function(e) {
            e.preventDefault();
            document.getElementById("printForm").submit();
        });

        
        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('KartuKeluargaController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@endsection