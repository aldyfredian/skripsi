@extends('admin.layouts.app')

@section('content')
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/master/kartu_keluarga')}}">Kartu Keluarga</a>
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data Keluarga</h3>
                                </div>
                                <div class="block-content">
                                @if($action=='kartu_keluarga.store')
                                  {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                @else
                                  {!! Form::open(['route' => [$action, $kk->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                @endif
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    <input value="{{ $kk->no_kk }}" class="form-control" type="text" id="noKK" name="noKK">
                                                    <label for="noKK">No. Kartu Keluarga</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    <input value="{{ $kk->kepala_keluarga }}" class="form-control" type="text" id="kepalaKeluarga" name="kepalaKeluarga">
                                                    <label for="kepalaKeluarga">Nama Kepala Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    <input value="{{ $kk->alamat }}" class="form-control" type="text" id="alamat" name="alamat">
                                                    <label for="alamat">Alamat</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-1">
                                                <div class="form-material floating">
                                                    <input value="{{ $kk->rt }}"class="form-control" type="text" id="rt" name="rt">
                                                    <label for="rt">RT</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-1">
                                                <div class="form-material floating">
                                                    <input value="{{ $kk->rw }}"class="form-control" type="text" id="rw" name="rw">
                                                    <label for="rw">RW</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-2">
                                                <div class="form-material floating">
                                                    {!! Form::select('kelurahan', [
                                                        'Ancol' => 'Ancol',
                                                        'Balonggede' => 'Balonggede',
                                                        'Ciateul' => 'Ciateul',
                                                        'Cigereleng' => 'Cigereleng',
                                                        'Ciseureuh' => 'Ciseureuh',
                                                        'Pasirluyu' => 'Pasirluyu',
                                                        'Pungkur' => 'Pungkur'], $kk->kelurahan, ['class' => 'form-control']) !!}
                                                    <label for="kelurahan">Please Select</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-plus push-5-r"></i> Register Data</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
@endsection