@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>
        <?php use App\Penduduk; use App\KartuKeluarga; ?>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading" style="text-transform: capitalize;">
                                Data Pindah {{$type}}
                            </h1>
                            <ol class="breadcrumb push-10-t" style="text-transform: uppercase;">
                                <li>Data Kependudukan</li>
                                <li><a class="link-effect" href="{{URL::to('admin/pindah/'.$type)}}">Data Pindah {{$type}}</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a data-toggle="modal" data-target="#modal-popin" type="button" class="btn btn-info btn-rounded">
                                <i class="fa fa-filter"></i> Filter Data
                            </a>
                            @if(Auth::user()->role == 'Admin')
                            <a id="print-btn" type="button" class="btn btn-success btn-rounded">
                                <i class="fa fa-print"></i> Print Data
                            </a>
                            @endif
                            <a href="{{URL::to('admin/pindah/'.$type.'/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>No. Registrasi</th>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        @if($type == 'datang')
                                        <th>Asal</th>
                                        @else
                                        <th>Tujuan</th>
                                        @endif
                                        <th>Kelurahan</th>
                                        <th>Status</th>
                                        <th width="200px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

@if($type == 'datang')
    {!! Form::open([
            'method' => 'delete', 
            'route' => ['datang.destroy', null],
            'class' => 'hiddenDeleteForm'
    ])!!}
    {!! Form::close() !!}
    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('start', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-awal']) !!}
                                    <label>Tanggal Registrasi (Awal)</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('end', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-akhir']) !!}
                                    <label>Tanggal Registrasi (Akhir)</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6">
                            <div class="form-material floating">
                                {!! Form::text('noreg', null, ['class' => 'form-control', 'id' => 'filter-no-reg']) !!}
                                <label>No. Registrasi</label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-material floating">
                                {!! Form::text('nokk', null, ['class' => 'form-control', 'id' => 'filter-no-kk']) !!}
                                <label>No. Kartu Keluarga</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nik', null, ['class' => 'form-control', 'id' => 'filter-nik']) !!}
                                    <label>NIK</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'id' => 'filter-nama']) !!}
                                    <label>Nama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">                                
                                <div class="form-material floating">
                                    {!! Form::select('jk', ['Semua' => 'Semua', 'Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control', 'id' => 'filter-jk', 'placeholder' => '']) !!}
                                    <label>Jenis Kelamin</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('kewarganegaraan', ['Semua' => 'Semua', 'WNI' => 'WNI', 'WNA' => 'WNA'], null, ['class' => 'form-control', 'id' => 'filter-kewarganegaraan', 'placeholder' => '']) !!}
                                    <label>Kewarganegaraan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('agama', [
                                        'Semua' => 'Semua',
                                        'Islam' => 'Islam',
                                        'Kristen' => 'Kristen',
                                        'Katolik' => 'Katolik',
                                        'Hindu' => 'Hindu',
                                        'Budha' => 'Budha',
                                        'Lainnya' => 'Lainnya'], null, ['class' => 'form-control', 'id' => 'filter-agama', 'placeholder' => '']) !!}
                                    <label>Agama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('kotaAsal', null, ['class' => 'form-control', 'id' => 'filter-kota-asal']) !!}
                                    <label>Kota Asal</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('kecAsal', null, ['class' => 'form-control', 'id' => 'filter-kecamatan-asal']) !!}
                                    <label>Kecamatan Asal</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamatLama', null, ['class' => 'form-control', 'id' => 'filter-alamat-lama']) !!}
                                    <label>Alamat Lama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamatBaru', null, ['class' => 'form-control', 'id' => 'filter-alamat-baru']) !!}
                                    <label>Alamat Baru</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        'Ancol' => 'Ancol',
                                        'Balonggede' => 'Balonggede',
                                        'Ciateul' => 'Ciateul',
                                        'Cigereleng' => 'Cigereleng',
                                        'Ciseureuh' => 'Ciseureuh',
                                        'Pasirluyu' => 'Pasirluyu',
                                        'Pungkur' => 'Pungkur'], null, ['class' => 'form-control', 'id' => 'filter-kelurahan', 'placeholder' => '']) !!}
                                    <label>Kelurahan</label>
                                </div>
                            </div>     
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        '0' => 'Ditunda',
                                        '1' => 'Diproses',
                                        '2' => 'Selesai'], null, ['class' => 'form-control', 'id' => 'filter-status', 'placeholder' => '']) !!}
                                    <label>Status</label>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->    
    {!! Form::open(['action' => 'PindahDatangController@print', 'id' => 'printForm', 'method' => 'POST']) !!}
        {!! Form::hidden('nama', null, ['id' => 'print-nama']) !!}
        {!! Form::hidden('nik', null, ['id' => 'print-nik']) !!}
        {!! Form::hidden('noreg', null, ['id' => 'print-noreg']) !!}
        {!! Form::hidden('nokk', null, ['id' => 'print-no-kk']) !!}
        {!! Form::hidden('tglAwal', null, ['id' => 'print-tanggal-awal']) !!}
        {!! Form::hidden('tglAkhir', null, ['id' => 'print-tanggal-akhir']) !!}
        {!! Form::hidden('jk', null, ['id' => 'print-jk']) !!}
        {!! Form::hidden('warga', null, ['id' => 'print-warga']) !!}
        {!! Form::hidden('agama', null, ['id' => 'print-agama']) !!}
        {!! Form::hidden('kotaAsal', null, ['id' => 'print-kota-asal']) !!}
        {!! Form::hidden('kecAsal', null, ['id' => 'print-kec-asal']) !!}
        {!! Form::hidden('kelurahan', null, ['id' => 'print-kelurahan']) !!}
        {!! Form::hidden('alamatLama', null, ['id' => 'print-alamat-lama']) !!}
        {!! Form::hidden('alamatBaru', null, ['id' => 'print-alamat-baru']) !!}
        {!! Form::hidden('status', null, ['id' => 'print-status']) !!}
    {!! Form::close() !!}
  <script type="text/javascript">
    $(document).ready(function() {
        function buildhidden() {
            document.getElementById("print-nama").value = $('#filter-nama').val();
            document.getElementById("print-nik").value = $('#filter-nik').val();
            document.getElementById("print-noreg").value = $('#filter-no-reg').val();
            document.getElementById("print-tanggal-awal").value = $('#filter-tgl-awal').val();
            document.getElementById("print-tanggal-akhir").value = $('#filter-tgl-akhir').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-warga").value = $('#filter-kewarganegaraan').val();
            document.getElementById("print-agama").value = $('#filter-agama').val();
            document.getElementById("print-kota-asal").value = $('#filter-kota-asal').val();
            document.getElementById("print-kec-asal").value = $('#filter-kecamatan-asal').val();
            document.getElementById("print-kelurahan").value = $('#filter-kelurahan').val();
            document.getElementById("print-alamat-lama").value = $('#filter-alamat-lama').val();
            document.getElementById("print-alamat-baru").value = $('#filter-alamat-baru').val();
            document.getElementById("print-status").value = $('#filter-status').val();
        }

        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
        });

        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "no_registrasi" },
                { "data": "nik" },
                { "data": "nama" },
                { "data": "asal" },
                { "data": "kelurahan" },
                { "data": "status" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('PindahDatangController@getDatatableDatangs') }}',
              "type": "POST",
              "data": function(d) {
                   d.noreg = $('#filter-no-reg').val(),
                   d.nokk = $('#filter-no-kk').val(),
                   d.nama = $('#filter-nama').val(),
                   d.nik = $('#filter-nik').val(),
                   d.tglAwal = $('#filter-tgl-awal').val(),
                   d.tglAkhir = $('#filter-tgl-akhir').val(),
                   d.jk = $('#filter-jk').val(),
                   d.kecAsal = $('#filter-kecamatan-asal').val(),
                   d.kotaAsal = $('#filter-kota-asal').val(),
                   d.warga = $('#filter-kewarganegaraan').val(),
                   d.agama = $('#filter-agama').val(),
                   d.alamatLama = $('#filter-alamat-lama').val(),
                   d.alamatBaru = $('#filter-alamat-baru').val(),
                   d.kelurahan = $('#filter-kelurahan').val(),
                   d.status = $('#filter-status').val()
              }
            },
            deferRender: true
        });
        
        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            buildhidden();
            t.draw();
        });
        

        $('#print-btn').click(function(e) {
            e.preventDefault();
            document.getElementById("printForm").submit();
        });

        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('PindahDatangController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@else
    {!! Form::open([
            'method' => 'delete', 
            'route' => ['pergi.destroy', null],
            'class' => 'hiddenDeleteForm'
    ])!!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('start', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-awal']) !!}
                                    <label>Tanggal Registrasi (Awal)</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('end', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-akhir']) !!}
                                    <label>Tanggal Registrasi (Akhir)</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6">
                            <div class="form-material floating">
                                {!! Form::text('noreg', null, ['class' => 'form-control', 'id' => 'filter-no-reg']) !!}
                                <label>No. Registrasi</label>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-material floating">
                                {!! Form::text('nokk', null, ['class' => 'form-control', 'id' => 'filter-no-kk']) !!}
                                <label>No. Kartu Keluarga</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nik', null, ['class' => 'form-control', 'id' => 'filter-nik']) !!}
                                    <label>NIK</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'id' => 'filter-nama']) !!}
                                    <label>Nama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">                                
                                <div class="form-material floating">
                                    {!! Form::select('jk', ['Semua' => 'Semua', 'Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control', 'id' => 'filter-jk', 'placeholder' => '']) !!}
                                    <label>Jenis Kelamin</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('kewarganegaraan', ['Semua' => 'Semua', 'WNI' => 'WNI', 'WNA' => 'WNA'], null, ['class' => 'form-control', 'id' => 'filter-kewarganegaraan', 'placeholder' => '']) !!}
                                    <label>Kewarganegaraan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('agama', [
                                        'Semua' => 'Semua',
                                        'Islam' => 'Islam',
                                        'Kristen' => 'Kristen',
                                        'Katolik' => 'Katolik',
                                        'Hindu' => 'Hindu',
                                        'Budha' => 'Budha',
                                        'Lainnya' => 'Lainnya'], null, ['class' => 'form-control', 'id' => 'filter-agama', 'placeholder' => '']) !!}
                                    <label>Agama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('kotaTujuan', null, ['class' => 'form-control', 'id' => 'filter-kota-tujuan']) !!}
                                    <label>Kota Tujuan</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('kecTujuan', null, ['class' => 'form-control', 'id' => 'filter-kecamatan-tujuan']) !!}
                                    <label>Kecamatan Tujuan</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamatLama', null, ['class' => 'form-control', 'id' => 'filter-alamat-lama']) !!}
                                    <label>Alamat Lama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamatBaru', null, ['class' => 'form-control', 'id' => 'filter-alamat-baru']) !!}
                                    <label>Alamat Baru</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        'Ancol' => 'Ancol',
                                        'Balonggede' => 'Balonggede',
                                        'Ciateul' => 'Ciateul',
                                        'Cigereleng' => 'Cigereleng',
                                        'Ciseureuh' => 'Ciseureuh',
                                        'Pasirluyu' => 'Pasirluyu',
                                        'Pungkur' => 'Pungkur'], null, ['class' => 'form-control', 'id' => 'filter-kelurahan', 'placeholder' => '']) !!}
                                    <label>Kelurahan</label>
                                </div>
                            </div>     
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        '0' => 'Ditunda',
                                        '1' => 'Diproses',
                                        '2' => 'Selesai'], null, ['class' => 'form-control', 'id' => 'filter-status', 'placeholder' => '']) !!}
                                    <label>Status</label>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->
    {!! Form::open(['action' => 'PindahPergiController@print', 'id' => 'printForm', 'method' => 'POST']) !!}
        {!! Form::hidden('nama', null, ['id' => 'print-nama']) !!}
        {!! Form::hidden('nik', null, ['id' => 'print-nik']) !!}
        {!! Form::hidden('noreg', null, ['id' => 'print-noreg']) !!}
        {!! Form::hidden('nokk', null, ['id' => 'print-no-kk']) !!}
        {!! Form::hidden('tglAwal', null, ['id' => 'print-tanggal-awal']) !!}
        {!! Form::hidden('tglAkhir', null, ['id' => 'print-tanggal-akhir']) !!}
        {!! Form::hidden('jk', null, ['id' => 'print-jk']) !!}
        {!! Form::hidden('warga', null, ['id' => 'print-warga']) !!}
        {!! Form::hidden('agama', null, ['id' => 'print-agama']) !!}
        {!! Form::hidden('kotaTujuan', null, ['id' => 'print-kota-tujuan']) !!}
        {!! Form::hidden('kecTujuan', null, ['id' => 'print-kec-tujuan']) !!}
        {!! Form::hidden('kelurahan', null, ['id' => 'print-kelurahan']) !!}
        {!! Form::hidden('alamatLama', null, ['id' => 'print-alamat-lama']) !!}
        {!! Form::hidden('alamatBaru', null, ['id' => 'print-alamat-baru']) !!}
        {!! Form::hidden('status', null, ['id' => 'print-status']) !!}
    {!! Form::close() !!}
<script type="text/javascript">
    $(document).ready(function() {
        function buildhidden() {
            document.getElementById("print-nama").value = $('#filter-nama').val();
            document.getElementById("print-nik").value = $('#filter-nik').val();
            document.getElementById("print-noreg").value = $('#filter-no-reg').val();
            document.getElementById("print-tanggal-awal").value = $('#filter-tgl-awal').val();
            document.getElementById("print-tanggal-akhir").value = $('#filter-tgl-akhir').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-warga").value = $('#filter-kewarganegaraan').val();
            document.getElementById("print-agama").value = $('#filter-agama').val();
            document.getElementById("print-kota-tujuan").value = $('#filter-kota-tujuan').val();
            document.getElementById("print-kec-tujuan").value = $('#filter-kecamatan-tujuan').val();
            document.getElementById("print-kelurahan").value = $('#filter-kelurahan').val();
            document.getElementById("print-alamat-lama").value = $('#filter-alamat-lama').val();
            document.getElementById("print-alamat-baru").value = $('#filter-alamat-baru').val();
            document.getElementById("print-status").value = $('#filter-status').val();
        }

        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
        });

        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "no_registrasi" },
                { "data": "nik" },
                { "data": "nama" },
                { "data": "tujuan" },
                { "data": "kelurahan" },
                { "data": "status" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('PindahPergiController@getDatatablePergis') }}',
              "type": "POST",
              "data": function(d) {
                   d.noreg = $('#filter-no-reg').val(),
                   d.nokk = $('#filter-no-kk').val(),
                   d.nama = $('#filter-nama').val(),
                   d.nik = $('#filter-nik').val(),
                   d.tglAwal = $('#filter-tgl-awal').val(),
                   d.tglAkhir = $('#filter-tgl-akhir').val(),
                   d.jk = $('#filter-jk').val(),
                   d.kecTujuan = $('#filter-kecamatan-tujuan').val(),
                   d.kotaTujuan = $('#filter-kota-tujuan').val(),
                   d.warga = $('#filter-kewarganegaraan').val(),
                   d.agama = $('#filter-agama').val(),
                   d.alamatLama = $('#filter-alamat-lama').val(),
                   d.alamatBaru = $('#filter-alamat-baru').val(),
                   d.kelurahan = $('#filter-kelurahan').val(),
                   d.status = $('#filter-status').val()
              }
            },
            deferRender: true
        });
        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            buildhidden();
            t.draw();
        });

        $('#print-btn').click(function(e) {
            e.preventDefault();
            document.getElementById("printForm").submit();
        });

        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('PindahPergiController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@endif
@endsection