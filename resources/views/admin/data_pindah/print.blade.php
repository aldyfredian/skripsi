@extends('admin.layouts.print.app')

@section('printBody')
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>Periode: {{ date("d/m/Y", strtotime($filter->tglAwal)).' - '.date("d/m/Y", strtotime($filter->tglAkhir))}}</h5>
        </div>
        @if(isset($filter->kelurahan) && $filter->kelurahan != "Semua")
        <div class="col-xs-4">
          <h5>Kelurahan: {{ $filter->kelurahan }}</h5>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5 style="font-weight: bold;">Jumlah: {{ $count }} Data </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>No. SKK</th>
                    <th>NIK / Nama</th>
                    <th>Jenis Kelamin</th>
                    @if($type == 'datang')
                      <th>Asal</th>
                      <th>Alamat Baru</th>
                    @else
                      <th>Tujuan</th>
                      <th>Alamat Lama</th>
                    @endif
                    <th>Kelurahan</th>
                </tr>
            </thead>
            <tbody>
              @foreach($q as $data)
              <tr>
                <td>{{ $data->no_registrasi }}</td>
                <td>{{ $data->nik.' - '.$data->nama }}</td>
                <td>{{ $data->jenis_kelamin }}</td>

                @if($type == 'datang')
                  <td>{{ $data->kota_asal.', '.$data->kecamatan_asal}}</td>
                  <td>{{ $data->alamat_baru }}</td>
                @else
                  <td>{{ $data->kota_tujuan.', '.$data->kecamatan_tujuan}}</td>
                  <td>{{ $data->alamat_lama }}</td>
                @endif

                <td>{{ $data->kelurahan }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection