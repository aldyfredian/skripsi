@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Data Kelahiran
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Data Kependudukan</li>
                                <li><a class="link-effect" href="{{URL::to('admin/kelahiran')}}">Data Kelahiran</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a data-toggle="modal" data-target="#modal-popin" type="button" class="btn btn-info btn-rounded">
                                <i class="fa fa-filter"></i> Filter Data
                            </a>
                            @if(Auth::user()->role == 'Admin')
                            <a id="print-btn" type="button" class="btn btn-success btn-rounded" target="_blank">
                                <i class="fa fa-print"></i> Print Data
                            </a>
                            @endif
                            <a href="{{URL::to('admin/kelahiran/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>No. SKK</th>
                                        <th>Nama Anak / JK</th>
                                        <th>Waktu Kelahiran</th>
                                        <th>Alamat</th>
                                        <th>Kelurahan</th>
                                        <th>Status</th>
                                        <th width="200px">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open([
            'method' => 'delete', 
            'route' => ['kelahiran.destroy', null],
            'class' => 'hiddenDeleteForm'
    ])!!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('start', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Mulai Dari (Tanggal Registrasi)', 'id' => 'filter-tgl-awal']) !!}
                                    <label>Tanggal Registrasi</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('end', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Hingga (Tanggal Registrasi)', 'id' => 'filter-tgl-akhir']) !!}
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'placeholder' => 'Nama', 'id' => 'filter-nama', 'onKeyUp' => 'input()']) !!}
                                    <label>Nama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('tempatLahir', null, ['class' => 'form-control', 'placeholder' => 'Tempat Lahir', 'id' => 'filter-tempat-lahir']) !!}
                                    <label>Tempat Lahir</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">                                
                                <div class="form-material">
                                    {!! Form::text('lahirstart', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Mulai Dari (Tanggal Lahir)', 'id' => 'filter-lahir-awal']) !!}
                                    <label>Tanggal Lahir</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('lahirend', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Hingga (Tanggal Lahir)', 'id' => 'filter-lahir-akhir']) !!}
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">                                
                                <div class="form-material">
                                    {!! Form::select('jk', ['Semua' => 'Semua', 'Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control','id' => 'filter-jk']) !!}
                                    <label>Jenis Kelamin</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    {!! Form::select('kewarganegaraan', ['Semua' => 'Semua', 'WNI' => 'WNI', 'WNA' => 'WNA'], null, ['class' => 'form-control', 'id' => 'filter-kewarganegaraan']) !!}
                                    <label>Kewarganegaraan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material">
                                    {!! Form::select('agama', [
                                        'Semua' => 'Semua',
                                        'Islam' => 'Islam',
                                        'Kristen' => 'Kristen',
                                        'Katolik' => 'Katolik',
                                        'Hindu' => 'Hindu',
                                        'Budha' => 'Budha',
                                        'Lainnya' => 'Lainnya'], null, ['class' => 'form-control', 'id' => 'filter-agama']) !!}
                                    <label>Agama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('ayah', null, ['class' => 'form-control', 'placeholder' => 'Nama Ayah', 'id' => 'filter-ayah']) !!}
                                    <label>Nama Ayah</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('ibu', null, ['class' => 'form-control','placeholder' => 'Nama Ibu', 'id' => 'filter-ibu']) !!}
                                    <label>Nama Ibu</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('tempatNikah', null, ['class' => 'form-control','placeholder' => 'Tempat Pernikahan', 'id' => 'filter-tempat-nikah']) !!}
                                    <label>Tempat Pernikahan</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('noBukuNikah', null, ['class' => 'form-control','placeholder' => 'No. Buku Nikah', 'id' => 'filter-buku-nikah']) !!}
                                    <label>No. Buku Nikah</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('nikahStart', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Mulai Dari (Tanggal Pernikahan)', 'id' => 'filter-tgl-nikah-awal']) !!}
                                    <label>Tanggal Pernikahan</label>
                                </div>
                            </div>                         
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('nikahend', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'placeholder' => 'Hingga (Tanggal Pernikahan)', 'id' => 'filter-tgl-nikah-akhir']) !!}
                                    <label>&nbsp;</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-6">
                                <div class="form-material">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-alamat', 'placeholder' => 'Alamat']) !!}
                                    <label>Alamat</label>
                                </div>
                            </div>                         
                            <div class="col-md-3">
                                <div class="form-material">
                                    {!! Form::text('rt', null, ['class' => 'form-control', 'id' => 'filter-rt', 'placeholder' => 'RT']) !!}
                                    <label>RT</label>
                                </div>
                            </div>                 
                            <div class="col-md-3">
                                <div class="form-material">
                                    {!! Form::text('rw', null, ['class' => 'form-control','id' => 'filter-rw', 'placeholder' => 'RW']) !!}
                                    <label>RW</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        'Ancol' => 'Ancol',
                                        'Balonggede' => 'Balonggede',
                                        'Ciateul' => 'Ciateul',
                                        'Cigereleng' => 'Cigereleng',
                                        'Ciseureuh' => 'Ciseureuh',
                                        'Pasirluyu' => 'Pasirluyu',
                                        'Pungkur' => 'Pungkur'], null, ['class' => 'form-control', 'id' => 'filter-kelurahan']) !!}
                                    <label>Kelurahan</label>
                                </div>
                            </div>     
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material">
                                    {!! Form::select('status', [
                                        'Semua' => 'Semua',
                                        '0' => 'Ditunda',
                                        '1' => 'Diproses',
                                        '2' => 'Selesai',], null, ['class' => 'form-control', 'id' => 'filter-status']) !!}
                                    <label>Status</label>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->

    {!! Form::open(['action' => 'KelahiranController@print', 'id' => 'printForm', 'method' => 'POST']) !!}
        {!! Form::hidden('nama', null, ['id' => 'print-nama']) !!}
        {!! Form::hidden('tglAwal', null, ['id' => 'print-start']) !!}
        {!! Form::hidden('tglAkhir', null, ['id' => 'print-end']) !!}
        {!! Form::hidden('tempatLahir', null, ['id' => 'print-tmlahir']) !!}
        {!! Form::hidden('lhrAwal', null, ['id' => 'print-lhrawal']) !!}
        {!! Form::hidden('lhrAkhir', null, ['id' => 'print-lhrakhir']) !!}
        {!! Form::hidden('jk', null, ['id' => 'print-jk']) !!}
        {!! Form::hidden('warga', null, ['id' => 'print-warga']) !!}
        {!! Form::hidden('agama', null, ['id' => 'print-agama']) !!}
        {!! Form::hidden('ayah', null, ['id' => 'print-ayah']) !!}
        {!! Form::hidden('ibu', null, ['id' => 'print-ibu']) !!}
        {!! Form::hidden('tmNikah', null, ['id' => 'print-tmnikah']) !!}
        {!! Form::hidden('bkNikah', null, ['id' => 'print-bknikah']) !!}
        {!! Form::hidden('nikahAwal', null, ['id' => 'print-nkawal']) !!}
        {!! Form::hidden('nikahAkhir', null, ['id' => 'print-nkakhir']) !!}
        {!! Form::hidden('kelurahan', null, ['id' => 'print-kelurahan']) !!}
        {!! Form::hidden('alamat', null, ['id' => 'print-alamat']) !!}
        {!! Form::hidden('rt', null, ['id' => 'print-rt']) !!}
        {!! Form::hidden('rw', null, ['id' => 'print-rw']) !!}
        {!! Form::hidden('status', null, ['id' => 'print-status']) !!}
    {!! Form::close() !!}
  <script type="text/javascript">
    $(document).ready(function() {
        function buildhidden() {
            document.getElementById("print-nama").value = $('#filter-nama').val();
            document.getElementById("print-start").value = $('#filter-tgl-awal').val();
            document.getElementById("print-end").value = $('#filter-tgl-akhir').val();
            document.getElementById("print-tmlahir").value = $('#filter-tempat-lahir').val();
            document.getElementById("print-lhrawal").value = $('#filter-lahir-awal').val();
            document.getElementById("print-lhrakhir").value = $('#filter-lahir-akhir').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-warga").value = $('#filter-kewarganegaraan').val();
            document.getElementById("print-agama").value = $('#filter-agama').val();
            document.getElementById("print-ayah").value = $('#filter-ayah').val();
            document.getElementById("print-ibu").value = $('#filter-ibu').val();
            document.getElementById("print-tmnikah").value = $('#filter-tempat-nikah').val();
            document.getElementById("print-bknikah").value = $('#filter-buku-nikah').val();
            document.getElementById("print-nkawal").value = $('#filter-tgl-nikah-awal').val();
            document.getElementById("print-nkakhir").value = $('#filter-tgl-nikah-akhir').val();
            document.getElementById("print-kelurahan").value = $('#filter-kelurahan').val();
            document.getElementById("print-alamat").value = $('#filter-alamat').val();
            document.getElementById("print-rt").value = $('#filter-rt').val();
            document.getElementById("print-rw").value = $('#filter-rw').val();
            document.getElementById("print-status").value = $('#filter-status').val();
        }

        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
        });

        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "no_skk" },
                { "data": "nama" },
                { "data": "waktu_kelahiran" },
                { "data": "alamat" },
                { "data": "kelurahan" },
                { "data": "status" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('KelahiranController@getDatatableKelahirans') }}',
              "type": "POST",
              "data": function(d) {
                   d.nama = $('#filter-nama').val(),
                   d.tglAwal = $('#filter-tgl-awal').val(),
                   d.tglAkhir = $('#filter-tgl-akhir').val(),
                   d.tempatLahir = $('#filter-tempat-lahir').val(),
                   d.lhrAwal = $('#filter-lahir-awal').val(),
                   d.lhrAkhir = $('#filter-lahir-akhir').val(),
                   d.jk = $('#filter-jk').val(),
                   d.warga = $('#filter-kewarganegaraan').val(),
                   d.agama = $('#filter-agama').val(),
                   d.ayah = $('#filter-ayah').val(),
                   d.ibu = $('#filter-ibu').val(),
                   d.tmNikah = $('#filter-tempat-nikah').val(),
                   d.bkNikah = $('#filter-buku-nikah').val(),
                   d.nikahAwal = $('#filter-tgl-nikah-awal').val(),
                   d.nikahAkhir = $('#filter-tgl-nikah-akhir').val(),
                   d.alamat = $('#filter-alamat').val(),
                   d.rt = $('#filter-rt').val(),
                   d.rw = $('#filter-rw').val(),
                   d.kelurahan = $('#filter-kelurahan').val()
                   d.status = $('#filter-status').val()
              }
            },
            deferRender: true
        });

        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            buildhidden();
            t.draw();
        });

        $('#print-btn').click(function(e) {
            e.preventDefault();
            document.getElementById("printForm").submit();
        });

        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('KelahiranController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@endsection