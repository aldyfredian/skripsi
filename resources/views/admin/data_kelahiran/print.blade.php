@extends('admin.layouts.print.app')

@section('printBody')
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>
            Periode: 
              @if(isset($filter->tglAwal)){{ date("d/m/Y", strtotime($filter->tglAwal)) }} @else *  @endif
              -
              @if(isset($filter->tglAkhir)){{ date("d/m/Y", strtotime($filter->tglAkhir)) }} @else * @endif
          </h5>
        </div>
        @if(isset($filter->kelurahan) && $filter->kelurahan != "Semua")
        <div class="col-xs-4">
          <h5>Kelurahan: {{ $filter->kelurahan }}</h5>
        </div>
        @endif
        @if(isset($filter->status) && $filter->status != "Semua")
        @if($filter->status == 0)
          @php $s = "Ditunda"; @endphp
        @endif
        @if($filter->status == 1)
          @php $s = "Diproses"; @endphp
        @endif
        @if($filter->status == 2)
          @php $s = "Selesai"; @endphp
        @endif
        <div class="col-xs-4">
          <h5>Status: {{ $s }}</h5>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5 style="font-weight: bold;">Jumlah: {{ $count }} Data </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>No. SKK</th>
                    <th>Nama Anak</th>
                    <th>Nama Orang Tua</th>
                    <th>Tanggal Kelahiran</th>
                    <th>Alamat</th>
                    <th>Kelurahan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
              @foreach($q as $data)
              <tr>
                <td>{{ $data->no_skk }}</td>
                <td>{{ $data->nama}}</td>
                <td>{{ $data->nama_ayah.' - '.$data->nama_ibu}}</td>
                <td>{{ date("d/m/Y", strtotime($data->waktu_kelahiran)) }}</td>
                <td>{{ $data->alamat.' RT '.$data->rt.' RW '.$data->rw}}</td>
                <td>{{ $data->kelurahan }}</td>
                @if($data->status == 0)
                  <td>Ditunda</td>
                @elseif($data->status == 1)
                  <td>Diproses</td>
                @elseif($data->status == 2)
                  <td>Selesai</td>
                @endif
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection