@extends('admin.layouts.print.app')

@section('printBody')
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>Periode: {{ date("d/m/Y", strtotime($filter->tglAwal)).' - '.date("d/m/Y", strtotime($filter->tglAkhir))}}</h5>
        </div>
        @if(isset($filter->kelurahan) && $filter->kelurahan != "Semua")
        <div class="col-xs-4">
          <h5>Kelurahan: {{ $filter->kelurahan }}</h5>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5 style="font-weight: bold;">Jumlah: {{ $count }} Data </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped" id="dataTable">
            <thead>
                <tr>
                    <th>No. SKK</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Waktu Kematian</th>
                    <th>Alamat</th>
                    <th>Kelurahan</th>
                </tr>
            </thead>
            <tbody>
              @foreach($kmt as $data)
              <tr>
                <td>{{ $data->no_skk }}</td>
                <td>{{ $data->nama }}</td>
                <td>{{ $data->jenis_kelamin}}</td>
                <td>{{ date("d/m/Y H:i:s", strtotime($data->waktu_kematian)) }}</td>
                <td>{{ $data->alamat.' RT '.$data->rt.' RW '.$data->rw}}</td>
                <td>{{ $data->kelurahan }}</td>
              </tr>
              @endforeach
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
@endsection