@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2-bootstrap.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/select2/select2.full.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                Data Penduduk
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li><a class="link-effect" href="{{URL::to('admin/master/data_penduduk')}}">Data Penduduk</a></li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a data-toggle="modal" data-target="#modal-popin" type="button" class="btn btn-info btn-rounded">
                                <i class="fa fa-filter"></i> Filter Data
                            </a>
                            @if(Auth::user()->role == 'Admin')
                            <a id="print-btn" type="button" class="btn btn-success btn-rounded">
                                <i class="fa fa-print"></i> Print Data
                            </a>
                            @endif
                            <a href="{{URL::to('admin/master/data_penduduk/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Alamat</th>
                                        <th>JK</th>
                                        <th>TTL</th>
                                        <th>Kelurahan</th>
                                        <th width="150px">Aksi</th>
                                    </tr>
                                </thead>

                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open(['method' => 'delete',
        'route' => ['data_penduduk.destroy', null],
        'class' => 'hiddenDeleteForm'])
    !!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('start', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-awal']) !!}
                                    <label>Tanggal Registrasi (Awal)</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('end', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-tgl-akhir']) !!}
                                    <label>Tanggal Registrasi (Akhir)</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">                                    
                                    {!! Form::text('nokk', null, ['class' => 'form-control', 'id' => 'filter-no-kk']) !!}
                                    <label>No. Kartu Keluarga</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('kepalaKeluarga', null, ['class' => 'form-control', 'id' => 'filter-kepala-keluarga']) !!}
                                    <label>Nama Kepala Keluarga</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">
                                <div class="form-material floating">                                    
                                    {!! Form::text('nik', null, ['class' => 'form-control', 'id' => 'filter-nik']) !!}
                                    <label>NIK</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::text('nama', null, ['class' => 'form-control', 'id' => 'filter-nama']) !!}
                                    <label>Nama Warga</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::text('tempatLahir', null, ['class' => 'form-control', 'id' => 'filter-tempat-lahir']) !!}
                                    <label>Tempat Lahir</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">                                
                                <div class="form-material floating">
                                    {!! Form::text('lahirstart', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-lahir-awal']) !!}
                                    <label>Tanggal Lahir (Awal)</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('lahirend', null, ['class' => 'form-control datepicker', 'data-date-format' => 'dd-mm-yyyy', 'id' => 'filter-lahir-akhir']) !!}
                                    <label>Tanggal Lahir (Akhir)</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('ayah', null, ['class' => 'form-control', 'id' => 'filter-ayah']) !!}
                                    <label>Nama Ayah</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('ibu', null, ['class' => 'form-control', 'id' => 'filter-ibu']) !!}
                                    <label>Nama Ibu</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">                                
                                <div class="form-material floating">
                                    {!! Form::select('jk', ['Semua' => 'Semua', 'Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], null, ['class' => 'form-control','placeholder' => '',  'id' => 'filter-jk']) !!}
                                    <label>Jenis Kelamin</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('kewarganegaraan', ['Semua' => 'Semua', 'WNI' => 'WNI', 'WNA' => 'WNA'], null, ['class' => 'form-control', 'placeholder' => '', 'id' => 'filter-kewarganegaraan']) !!}
                                    <label>Kewarganegaraan</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-material floating">
                                    {!! Form::select('agama', [
                                        'Semua' => 'Semua',
                                        'Islam' => 'Islam',
                                        'Kristen' => 'Kristen',
                                        'Katolik' => 'Katolik',
                                        'Hindu' => 'Hindu',
                                        'Budha' => 'Budha',
                                        'Lainnya' => 'Lainnya'], null, ['class' => 'form-control', 'placeholder' => '', 'id' => 'filter-agama']) !!}
                                    <label>Agama</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::select('pendidikan', [
                                        'Semua' => 'Semua',
                                        'Tidak / Belum Sekolah' => 'Tidak / Belum Sekolah',
                                        'Belum Tamat SD / Sederajat' => 'Belum Tamat SD / Sederajat',
                                        'Tamat SD / Sederajat' =>  'Tamat SD / Sederajat',
                                        'SLTP / Sederajat' => 'SLTP / Sederajat' ,
                                        'SLTA / Sederajat' => 'SLTA / Sederajat',
                                        'Diploma I / II' => 'Diploma I / II',
                                        'Akademi / Diploma III / Sarjana Muda' => 'Akademi / Diploma III / Sarjana Muda' ,
                                        'Diploma IV / Strata I' => 'Diploma IV / Strata I',
                                        'Strata II' => 'Strata II',
                                        'Strata III' => 'Strata III'], null, ['class'=>'form-control', 'placeholder' => '', 'id' => 'filter-pendidikan']) !!}
                                    <label for="pendidikan">Pendidikan Terakhir</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    @php $pekerjaan = App\Pekerjaan::all(); @endphp
                                    <select class="form-control" name="pekerjaan" id="filter-pekerjaan">
                                        <option selected="selected" disabled="disabled" hidden="hidden" value=""></option>
                                        <option value="Semua">Semua</option>
                                        @foreach($pekerjaan as $p)
                                        <option value="{{$p->id}}">{{$p->nama}}</option>
                                        @endforeach
                                    </select>
                                    <label for="pekerjaan">Pekerjaan</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::select('statusPernikahan', [
                                        'Semua' => 'Semua',
                                        'Belum Kawin' => 'Belum Kawin',
                                        'Kawin' => 'Kawin',
                                        'Cerai Hidup',
                                        'Cerai Mati'], null, ['class'=>'form-control', 'placeholder' => '', 'id' => 'filter-status-pernikahan']) !!}
                                    <label for="statusPernikahan">Status Perkawinan</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                 <div class="form-material floating">
                                    {!! Form::select('statusHubungan', [
                                        'Semua' => 'Semua',
                                        'Kepala Keluarga' => 'Kepala Keluarga',
                                        'Suami' => 'Suami',
                                        'Istri' => 'Istri',
                                        'Anak' => 'Anak',
                                        'Menantu' => 'Menantu',
                                        'Cucu' => 'Cucu',
                                        'Orang Tua' => 'Orang Tua',
                                        'Mertua' => 'Mertua',
                                        'Famili Lain' => 'Famili Lain',
                                        'Pembantu' => 'Pembantu',
                                        'Lainnya' => 'Lainnya'
                                        ], null, ['class' => 'form-control', 'placeholder' => '', 'id' => 'filter-status-hubungan']) !!}
                                    <label for="statusHubungan">Status Hubungan Dalam Keluarga</label>
                                </div>                                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-alamat']) !!}
                                    <label>Alamat</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-rt']) !!}
                                    <label>RT</label>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-material floating">
                                    {!! Form::text('alamat', null, ['class' => 'form-control', 'id' => 'filter-rw']) !!}
                                    <label>RW</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">                            
                            <div class="col-md-12">
                                <div class="form-material floating">
                                    {!! Form::select('kelurahan', [
                                        'Semua' => 'Semua',
                                        'Ancol' => 'Ancol',
                                        'Balonggede' => 'Balonggede',
                                        'Ciateul' => 'Ciateul',
                                        'Cigereleng' => 'Cigereleng',
                                        'Ciseureuh' => 'Ciseureuh',
                                        'Pasirluyu' => 'Pasirluyu',
                                        'Pungkur' => 'Pungkur'], null, ['class' => 'form-control', 'placeholder' => '', 'id' => 'filter-kelurahan']) !!}
                                    <label>Kelurahan</label>
                                </div>
                            </div>     
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->

    {!! Form::open(['action' => 'PendudukController@print', 'id' => 'printForm', 'method' => 'POST']) !!}
        {!! Form::hidden('nokk', null, ['id' => 'print-no-kk']) !!}
        {!! Form::hidden('kepalaKeluarga', null, ['id' => 'print-kepala-keluarga']) !!}
        {!! Form::hidden('nama', null, ['id' => 'print-nama']) !!}
        {!! Form::hidden('nik', null, ['id' => 'print-nik']) !!}
        {!! Form::hidden('tempatLahir', null, ['id' => 'print-tempat-lahir']) !!}
        {!! Form::hidden('lahirawal', null, ['id' => 'print-lahir-awal']) !!}
        {!! Form::hidden('lahirakhir', null, ['id' => 'print-lahir-akhir']) !!}
        {!! Form::hidden('ayah', null, ['id' => 'print-ayah']) !!}
        {!! Form::hidden('ibu', null, ['id' => 'print-ibu']) !!}
        {!! Form::hidden('jk', null, ['id' => 'print-jk']) !!}
        {!! Form::hidden('kewarganegaraan', null, ['id' => 'print-kewarganegaraan']) !!}
        {!! Form::hidden('agama', null, ['id' => 'print-agama']) !!}
        {!! Form::hidden('pendidikan', null, ['id' => 'print-pendidikan']) !!}
        {!! Form::hidden('pekerjaan', null, ['id' => 'print-pekerjaan']) !!}
        {!! Form::hidden('statusHubungan', null, ['id' => 'print-status-hubungan']) !!}
        {!! Form::hidden('statusPernikahan', null, ['id' => 'print-status-pernikahan']) !!}
        {!! Form::hidden('tglAwal', null, ['id' => 'print-tanggal-awal']) !!}
        {!! Form::hidden('tglAkhir', null, ['id' => 'print-tanggal-akhir']) !!}
        {!! Form::hidden('kelurahan', null, ['id' => 'print-kelurahan']) !!}
        {!! Form::hidden('alamat', null, ['id' => 'print-alamat']) !!}
        {!! Form::hidden('rt', null, ['id' => 'print-rt']) !!}
        {!! Form::hidden('rw', null, ['id' => 'print-rw']) !!}
    {!! Form::close() !!}

  <script type="text/javascript">
    $(document).ready(function() {
        function buildhidden() {
            document.getElementById("print-no-kk").value = $('#filter-no-kk').val();
            document.getElementById("print-kepala-keluarga").value = $('#filter-kepala-keluarga').val();
            document.getElementById("print-nama").value = $('#filter-nama').val();
            document.getElementById("print-nik").value = $('#filter-nik').val();
            document.getElementById("print-tempat-lahir").value = $('#filter-tempat-lahir').val();
            document.getElementById("print-lahir-awal").value = $('#filter-lahir-awal').val();
            document.getElementById("print-lahir-akhir").value = $('#filter-lahir-akhir').val();
            document.getElementById("print-ayah").value = $('#filter-ayah').val();
            document.getElementById("print-ibu").value = $('#filter-ibu').val();
            document.getElementById("print-jk").value = $('#filter-jk').val();
            document.getElementById("print-kewarganegaraan").value = $('#filter-kewarganegaraan').val();
            document.getElementById("print-agama").value = $('#filter-agama').val();
            document.getElementById("print-pendidikan").value = $('#filter-pendidikan').val();
            document.getElementById("print-pekerjaan").value = $('#filter-pekerjaan').val();
            document.getElementById("print-status-hubungan").value = $('#filter-status-hubungan').val();
            document.getElementById("print-status-pernikahan").value = $('#filter-status-pernikahan').val();
            document.getElementById("print-tanggal-awal").value = $('#filter-tgl-awal').val();
            document.getElementById("print-tanggal-akhir").value = $('#filter-tgl-akhir').val();
            document.getElementById("print-kelurahan").value = $('#filter-kelurahan').val();
            document.getElementById("print-alamat").value = $('#filter-alamat').val();
            document.getElementById("print-rt").value = $('#filter-rt').val();
            document.getElementById("print-rwr").value = $('#filter-rw').val();
        }

        $('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
        });

        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "nik" },
                { "data": "nama" },
                { "data": "alamat" },
                { "data": "jenis_kelamin" },
                { "data": "ttl" },
                { "data": "kelurahan" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('PendudukController@getDatatablePenduduks') }}',
              "type": "POST",
              "data": function(d) {
                   d.nokk = $('#filter-no-kk').val(),
                   d.kepalakeluarga = $('#filter-kepala-keluarga').val(),
                   d.nama = $('#filter-nama').val(),
                   d.nik = $('#filter-nik').val(),
                   d.tempatlahir = $('#filter-tempat-lahir').val(),
                   d.lahirawal = $('#filter-lahir-awal').val(),
                   d.lahirakhir = $('#filter-lahir-akhir').val(),
                   d.ayah = $('#filter-ayah').val(),
                   d.ibu = $('#filter-ibu').val(),
                   d.jk = $('#filter-jk').val(),
                   d.kewarganegaraan = $('#filter-kewarganegaraan').val(),
                   d.agama = $('#filter-agama').val(),
                   d.pendidikan = $('#filter-pendidikan').val(),
                   d.pekerjaan = $('#filter-pekerjaan').val(),
                   d.statusHubungan = $('#filter-status-hubungan').val(),
                   d.statusPernikahan = $('#filter-status-pernikahan').val(),
                   d.tglAwal = $('#filter-tgl-awal').val(),
                   d.tglAkhir = $('#filter-tgl-akhir').val(),
                   d.alamat = $('#filter-alamat').val(),
                   d.rt = $('#filter-rt').val(),
                   d.rw = $('#filter-rw').val(),
                   d.kelurahan = $('#filter-kelurahan').val()
              }
            },
            deferRender: true
        });

        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            t.draw();
            buildhidden();
        });
        
        $('#print-btn').click(function(e) {
            e.preventDefault();
            document.getElementById("printForm").submit();
        });

        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('PendudukController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@endsection