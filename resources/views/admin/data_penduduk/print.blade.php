@extends('admin.layouts.print.app')

@section('printBody')
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>Periode: {{ date("d/m/Y", strtotime($filter->tglAwal)).' - '.date("d/m/Y", strtotime($filter->tglAkhir))}}</h5>
        </div>
        @if(isset($filter->kelurahan) && $filter->kelurahan != "Semua")
        <div class="col-xs-4">
          <h5>Kelurahan: {{ $filter->kelurahan }}</h5>
        </div>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5 style="font-weight: bold;">Jumlah: {{ $count }} Data </h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>NIK</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>JK</th>
                        <th>TTL</th>
                        <th>Kelurahan</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($penduduk as $data)
                  <tr>
                    <td>{{ $data->nik }}</td>
                    <td>{{ $data->nama}}</td>
                    <td>{{ $data->alamat.' RT '.$data->rt.' RW '.$data->rw}}</td>
                    <td>{{ $data->jenis_kelamin }}</td>
                    <td>{{ $data->tempat_lahir.', '.date("d/m/Y", strtotime($data->tanggal_lahir)) }}</td>
                    <td>{{ $data->kelurahan }}</td>
                  </tr>
                  @endforeach
                </tbody>
            </table>
        </div>
      </div>
    </div>
  </div>
@endsection