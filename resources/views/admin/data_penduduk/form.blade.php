@extends('admin.layouts.app')

@section('content')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/select2/select2-bootstrap.min.css')}}">

        <script src="{{URL::to('assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/select2/select2.full.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/jquery-validation/additional-methods.min.js')}}"></script>

        <script>
            jQuery(function () {
                // Init page helpers (Select2 plugin)
                App.initHelpers('select2', 'datepicker');
            });
        </script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <h1 class="page-heading">
                                {{ $title }}
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li>Master Data</li>
                                <li>
                                    <a class="link-effect" href="{{URL::to('admin/master/data_penduduk')}}">Data Penduduk</a>
                                </li>
                                <li>
                                    <a class="link-effect">{{ $title }}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->
                <!-- Page Content -->
                <div class="content content-narrow">
                    <!-- Register Forms -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Material (floating) Register -->
                            <div class="block block-themed">
                                <div class="block-header bg-primary">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="content_toggle"></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Data Individual</h3>
                                </div>
                                <div class="block-content">
                                @if($action=='data_penduduk.store')
                                  {!! Form::open(['route' => $action, 'role' => 'form', 'class' => 'form-horizontal push-10-t push-10']) !!}
                                  @php $noKK = ""; @endphp
                                @else
                                  {!! Form::open(['route' => [$action, $penduduk->id], 'role' => 'form', 'method' => 'PATCH','class' => 'form-horizontal push-10-t push-10']) !!}
                                @endif
                                        <div class="form-group">
                                        <h4 class="form-subtitle">Data Kartu Keluarga</h4>
                                            <div class="col-xs-12">
                                                 <div class="form-material floating">
                                                    @if(isset($penduduk->id_kk))
                                                    @php $kk = App\KartuKeluarga::find($penduduk->id_kk); $markup = "( ".$kk->no_kk." ) ".$kk->kepala_keluarga." - ".$kk->alamat." RT ".$kk->rt." RW ".$kk->RW." Kel. ".$kk->kelurahan; @endphp
                                                    {!! Form::select('noKK', [$penduduk->id_kk => $markup], null, ['class' => 'js-select2 form-control kkList', 'required']) !!}
                                                    @else
                                                    {!! Form::select('noKK', [], null, ['class' => 'js-select2 form-control kkList', 'required']) !!}
                                                    @endif
                                                    <label for="noKK">Pilih Data Kartu Keluarga</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <h4 class="form-subtitle">Data Penduduk</h4>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    <input value="{{ $penduduk->nik }}" class="form-control" type="text" id="nik" name="nik">
                                                    <label for="nik">NIK</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    <input value="{{ $penduduk->nama }}" class="form-control" type="text" id="nama" name="nama">
                                                    <label for="nama">Nama Penduduk</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="form-material floating">
                                                    {!! Form::select('jk', ['Laki - Laki' => 'Laki - Laki', 'Perempuan' => 'Perempuan'], $penduduk->jenis_kelamin, ['class' => 'form-control']) !!}
                                                    <label for="jk">Jenis Kelamin</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('tempatLahir', $penduduk->tempat_lahir, ['class' => 'form-control']) !!}
                                                    <label for="tempatLahir">Tempat Lahir</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('tanggalLahir', date("d-m-Y", strtotime($penduduk->tanggal_lahir)), ['class'=>'datepicker form-control', 'data-date-format'=>"dd-mm-yy"]) !!}
                                                    <label for="tanggalLahir">Tanggal Lahir</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::select('pendidikan', [
                                                        'Tidak / Belum Sekolah' => 'Tidak / Belum Sekolah',
                                                        'Belum Tamat SD / Sederajat' => 'Belum Tamat SD / Sederajat',
                                                        'Tamat SD / Sederajat' =>  'Tamat SD / Sederajat',
                                                        'SLTP / Sederajat' => 'SLTP / Sederajat' ,
                                                        'SLTA / Sederajat' => 'SLTA / Sederajat',
                                                        'Diploma I / II' => 'Diploma I / II',
                                                        'Akademi / Diploma III / Sarjana Muda' => 'Akademi / Diploma III / Sarjana Muda' ,
                                                        'Diploma IV / Strata I' => 'Diploma IV / Strata I',
                                                        'Strata II' => 'Strata II',
                                                        'Strata III' => 'Strata III'], $penduduk->pendidikan, ['class'=>'form-control']) !!}
                                                    <label for="pendidikan">Pendidikan Terakhir</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    @if(isset($penduduk->pekerjaan))
                                                    @php $kerja = App\Pekerjaan::find($penduduk->pekerjaan); $m = $kerja->kode." - ".$kerja->nama; @endphp
                                                    {!! Form::select('pekerjaan', [$penduduk->pekerjaan => $m], null, ['class' => 'js-select2 form-control listPekerjaan', 'required']) !!}
                                                    @else
                                                    {!! Form::select('pekerjaan', [], null, ['class' => 'js-select2 form-control listPekerjaan', 'required']) !!}
                                                    @endif
                                                    <label for="pekerjaan">Pekerjaan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::select('statusPernikahan', [
                                                        'Belum Kawin' => 'Belum Kawin',
                                                        'Kawin' => 'Kawin',
                                                        'Cerai Hidup',
                                                        'Cerai Mati'], $penduduk->status_pernikahan, ['class'=>'form-control']) !!}
                                                    <label for="statusPernikahan">Status Perkawinan</label>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                 <div class="form-material floating">
                                                    {!! Form::select('statusHubungan', [
                                                        'Kepala Keluarga' => 'Kepala Keluarga',
                                                        'Suami' => 'Suami',
                                                        'Istri' => 'Istri',
                                                        'Anak' => 'Anak',
                                                        'Menantu' => 'Menantu',
                                                        'Cucu' => 'Cucu',
                                                        'Orang Tua' => 'Orang Tua',
                                                        'Mertua' => 'Mertua',
                                                        'Famili Lain' => 'Famili Lain',
                                                        'Pembantu' => 'Pembantu',
                                                        'Lainnya' => 'Lainnya'
                                                        ], $penduduk->status_hubungan, ['class' => 'form-control']) !!}
                                                    <label for="statusHubungan">Status Hubungan Dalam Keluarga</label>
                                                </div>                                                
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-6">
                                                 <div class="form-material floating">
                                                    {!! Form::select('agama', [
                                                        'Islam' => 'Islam',
                                                        'Kristen' => 'Kristen',
                                                        'Katolik' => 'Katolik',
                                                        'Hindu' => 'Hindu',
                                                        'Budha' => 'Budha',
                                                        'Lainnya' => 'Lainnya',
                                                        ], $penduduk->agama, ['class' => 'form-control']) !!}
                                                    <label for="agama">Agama</label>
                                                </div>                                                
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::select('kewarganegaraan', ['WNI'=>'WNI','WNA'=>'WNA'], $penduduk->kewarganegaraan, ['class' => 'form-control']) !!}
                                                    <label for="kewarganegaraan">Kewarganegaraan</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                        <h4 class="form-subtitle">Nama Orang Tua</h4>
                                            <div class="col-xs-6">                                                
                                                <div class="form-material floating">
                                                    {!! Form::text('namaAyah', $penduduk->nama_ayah, ['class'=>'form-control']) !!}
                                                    {!! Form::label('namaAyah', 'Nama Ayah', []) !!}
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-material floating">
                                                    {!! Form::text('namaIbu', $penduduk->nama_ibu, ['class'=>'form-control']) !!}
                                                    {!! Form::label('namaIbu', 'Nama Ibu', []) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-plus push-5-r"></i> Register Data</button>
                                            </div>
                                        </div>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <!-- END Material (floating) Register -->
                        </div>
                    </div>
                    <!-- END Register Forms -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

<script>
$(document).ready(function() {
    $('.datepicker').datepicker({
    format: 'dd-mm-yyyy'
    });

    /* Select 2 Kartu Keluarga*/
      $(".kkList").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('KartuKeluargaController@getKKs') }}",
          dataType: 'json',
          delay: 1,
          type: "get",
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: function(repo) {
          if (repo.loading) return repo.no_kk;
          var markup = "<div>( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan+"</div>";
          return markup;
        },
        templateSelection: function(repo) {
          return repo.text || "( "+repo.no_kk+" ) "+ repo.kepala_keluarga+" - "+repo.alamat+" RT "+repo.rt+" RW "+repo.rw+" Kel. "+repo.kelurahan;
        }

      });
      /*End select 2 Kartu Keluarga*/

    /* Select 2 Pekerjaan*/
      $(".listPekerjaan").select2({
        placeholder: " ",
        allowClear: true,

        ajax: {
          url: "{{ action('PekerjaanController@getPekerjaan') }}",
          dataType: 'json',
          delay: 2,
          type: "get",
          data: function (params) {
            return {
              q: params.term
            };
          },
          processResults: function (data, params) {
            return {
              results: data
            };
          },
          cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 2,
        templateResult: function(repo) {
          if (repo.loading) return repo.nama;
          var markup = "<div>"+repo.kode+" - "+ repo.nama+"</div>";
          return markup;
        },
        templateSelection: function(repo) {
          return repo.text || repo.kode+" - "+ repo.nama;
        }

      });
      /*End select 2 Pekerjaan*/
    });
</script>
@endsection