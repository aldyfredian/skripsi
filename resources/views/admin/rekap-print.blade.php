@extends('admin.layouts.print.app')  

@section('printBody')
  <style type="text/css">
    th, td{
      text-align: center;
    }
    .total > td{
      font-weight: bold;
      font-style: italic;
    }
  </style>
  <div class="row">    
      <div class="col-xs-12">
        <h5 style="text-align: center;">Berdasarkan: {{ $filter->type }}</h5>
      </div>
  </div>
  <div class="row">
    <div class="col-xs-12">
      <div class="row" style="margin-bottom: 10px;">
        <div class="col-xs-4">
          <h5>Periode: {{ date("d/m/Y", strtotime($filter->from)).' - '.date("d/m/Y", strtotime($filter->to))}}</h5>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          @if($filter->type == 'Jenis Kelamin')
          <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th rowspan="2" style="vertical-align: middle;">Kelurahan</th>
                  <th colspan="2" rowspan="2" style="vertical-align: middle;">Kartu Keluarga</th>
                  <th colspan="2">Data Penduduk</th>
                  <th colspan="2">Data Kelahiran</th>
                  <th colspan="2">Data Kematian</th>
                  <th colspan="2">Data Pindah Datang</th>
                  <th colspan="2">Data Pindah Pergi</th>
                  <th colspan="3">Jumlah</th>
                </tr>
                {{-- @if($filter->type == 'Agama') --}}
                <tr style="font-style: italic;">
                  @for($i = 1; $i < 7; $i++)
                    <th>L</th>
                    <th>P</th>
                  @endfor
                    <th>Total</th>
                </tr>
                {{-- @endif --}}
            </thead>
            <tbody>
              @php
                $KK = 0; 
                $DPL = 0; $DPP = 0;
                $KLRL = 0; $KLRP = 0;
                $KMTL = 0; $KMTP = 0;
                $DTGL = 0; $DTGP = 0;
                $PRGL = 0; $PRGP = 0;
                $TDDL = 0; $TDDP = 0;
                $TGT = 0;
              @endphp
              @foreach($REKAP as $key => $x)
                @php
                  $TDL = $x['DP']['L'] + $x['KLR']['L'] + $x['KMT']['L'] + $x['DTG']['L'] + $x['PRG']['L'];
                  $TDP = $x['DP']['P'] + $x['KLR']['P'] + $x['KMT']['P'] + $x['DTG']['P'] + $x['PRG']['P'];
                  $GT = $TDL + $TDP;
                @endphp            
                <tr>
                  <td>{{$key}}</td>
                  <td colspan="2">@if($x['KK'] == 0) {{'-'}} @else {{$x['KK']}} @endif</td>
                  <td>@if($x['DP']['L'] == 0) {{'-'}} @else {{$x['DP']['L']}} @endif</td>
                  <td>@if($x['DP']['P'] == 0) {{'-'}} @else {{$x['DP']['P']}} @endif</td>
                  <td>@if($x['KLR']['L'] == 0) {{'-'}} @else {{$x['KLR']['L']}} @endif</td>
                  <td>@if($x['KLR']['P'] == 0) {{'-'}} @else {{$x['KLR']['P']}} @endif</td>
                  <td>@if($x['KMT']['L'] == 0) {{'-'}} @else {{$x['KMT']['L']}} @endif</td>
                  <td>@if($x['KMT']['P'] == 0) {{'-'}} @else {{$x['KMT']['P']}} @endif</td>
                  <td>@if($x['DTG']['L'] == 0) {{'-'}} @else {{$x['DTG']['L']}} @endif</td>
                  <td>@if($x['DTG']['P'] == 0) {{'-'}} @else {{$x['DTG']['P']}} @endif</td>
                  <td>@if($x['PRG']['L'] == 0) {{'-'}} @else {{$x['PRG']['L']}} @endif</td>
                  <td>@if($x['PRG']['P'] == 0) {{'-'}} @else {{$x['PRG']['P']}} @endif</td>
                  <td>@if($TDL == 0) {{'-'}} @else {{$TDL}} @endif</td>
                  <td>@if($TDP == 0) {{'-'}} @else {{$TDP}} @endif</td>
                  <td>@if($GT == 0) {{'-'}} @else {{$GT}} @endif</td>
                </tr>
                @php
                 $KK = $x['KK'] + $KK;
                 $DPL = $x['DP']['L'] + $DPL;
                 $DPP = $x['DP']['P'] + $DPP;
                 $KLRL = $x['KLR']['L'] + $KLRL;
                 $KLRP = $x['KLR']['P'] + $KLRP;
                 $KMTL = $x['KMT']['L'] + $KMTL;
                 $KMTP = $x['KMT']['P'] + $KMTP;
                 $DTGL = $x['DTG']['L'] + $DTGL;
                 $DTGP = $x['DTG']['P'] + $DTGP;
                 $PRGL = $x['PRG']['L'] + $PRGL;
                 $PRGP = $x['PRG']['P'] + $PRGP;
                 $TDDL = $TDDL + $TDL;
                 $TDDP = $TDDP + $TDP;
                 $TGT = $TGT + $GT;
                @endphp
              @endforeach
              <tr class="total">
                <td>Jumlah</td>
                <td colspan="2">{{ $KK }}</td>
                <td>{{ $DPL }}</td>
                <td>{{ $DPP }}</td>
                <td>{{ $KLRL }}</td>
                <td>{{ $KLRP }}</td>
                <td>{{ $KMTL }}</td>
                <td>{{ $KMTP }}</td>
                <td>{{ $DTGL }}</td>
                <td>{{ $DTGP }}</td>
                <td>{{ $PRGL }}</td>
                <td>{{ $PRGP }}</td>
                <td>{{ $TDDL }}</td>
                <td>{{ $TDDP }}</td>
                <td>{{ $TGT }}</td>
              </tr>
            </tbody>
          </table>
          @endif
          @if($filter->type == 'Agama')
          <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th rowspan="2" style="vertical-align: middle;">Kelurahan</th>
                  <th colspan="6" rowspan="2" style="vertical-align: middle;">Kartu Keluarga</th>
                  <th colspan="6">Data Penduduk</th>
                  <th colspan="6">Data Kelahiran</th>
                  <th colspan="6">Data Kematian</th>
                  <th colspan="6">Data Pindah Datang</th>
                  <th colspan="6">Data Pindah Pergi</th>
                  <th colspan="7">Jumlah</th>
                </tr>
                {{-- @if($filter->type == 'Agama') --}}
                <tr style="font-style: italic;">
                  @for($i = 1; $i < 7; $i++)
                    <th>1</th>
                    <th>2</th>
                    <th>3</th>
                    <th>4</th>
                    <th>5</th>
                    <th>6</th>
                  @endfor
                    <td>Total</td>
                </tr>
                {{-- @endif --}}
            </thead>
            <tbody>
              @php
                $KK = 0;
                $DP1 = 0; $DP2 = 0; $DP3 = 0; $DP4 = 0; $DP5 = 0; $DP6 = 0;
                $KLR1 = 0; $KLR2 = 0; $KLR3 = 0; $KLR4 = 0; $KLR5 = 0; $KLR6 = 0;
                $KMT1 = 0; $KMT2 = 0; $KMT3 = 0; $KMT4 = 0; $KMT5 = 0; $KMT6 = 0;
                $DTG1 = 0; $DTG2 = 0; $DTG3 = 0; $DTG4 = 0; $DTG5 = 0; $DTG6 = 0;
                $PRG1 = 0; $PRG2 = 0; $PRG3 = 0; $PRG4 = 0; $PRG5 = 0; $PRG6 = 0;
                $JTD1 = 0; $JTD2 = 0; $JTD3 = 0; $JTD4 = 0; $JTD5 = 0; $JTD6 = 0;
                $JTDT = 0;
              @endphp
              @foreach($REKAP as $key => $x)
                @php 
                  $D1 = $x['DP']['1'] + $x['KLR']['1'] + $x['KMT']['1'] + $x['DTG']['1'] + $x['PRG']['1'];
                  $D2 = $x['DP']['2'] + $x['KLR']['2'] + $x['KMT']['2'] + $x['DTG']['2'] + $x['PRG']['2'];
                  $D3 = $x['DP']['3'] + $x['KLR']['3'] + $x['KMT']['3'] + $x['DTG']['3'] + $x['PRG']['3'];
                  $D4 = $x['DP']['4'] + $x['KLR']['4'] + $x['KMT']['4'] + $x['DTG']['4'] + $x['PRG']['4'];
                  $D5 = $x['DP']['5'] + $x['KLR']['5'] + $x['KMT']['5'] + $x['DTG']['5'] + $x['PRG']['5'];
                  $D6 = $x['DP']['6'] + $x['KLR']['6'] + $x['KMT']['6'] + $x['DTG']['6'] + $x['PRG']['6'];
                  $DT = $D1 + $D2 + $D3 + $D4 + $D5 + $D6;
                @endphp        
                <tr>
                  <td>{{$key}}</td>
                  <td colspan="6">@if($x['KK'] == 0) {{'-'}} @else {{$x['KK']}} @endif</td>
                  <td>@if($x['DP']['1'] == 0) {{'-'}} @else {{$x['DP']['1']}}@endif</td>
                  <td>@if($x['DP']['2'] == 0) {{'-'}} @else {{$x['DP']['2']}}@endif</td>
                  <td>@if($x['DP']['3'] == 0) {{'-'}} @else {{$x['DP']['3']}}@endif</td>
                  <td>@if($x['DP']['4'] == 0) {{'-'}} @else {{$x['DP']['4']}}@endif</td>
                  <td>@if($x['DP']['5'] == 0) {{'-'}} @else {{$x['DP']['5']}}@endif</td>
                  <td>@if($x['DP']['6'] == 0) {{'-'}} @else {{$x['DP']['6']}}@endif</td>
                  <td>@if($x['KLR']['1'] == 0) {{'-'}} @else {{$x['KLR']['1']}}@endif</td>
                  <td>@if($x['KLR']['2'] == 0) {{'-'}} @else {{$x['KLR']['2']}}@endif</td>
                  <td>@if($x['KLR']['3'] == 0) {{'-'}} @else {{$x['KLR']['3']}}@endif</td>
                  <td>@if($x['KLR']['4'] == 0) {{'-'}} @else {{$x['KLR']['4']}}@endif</td>
                  <td>@if($x['KLR']['5'] == 0) {{'-'}} @else {{$x['KLR']['5']}}@endif</td>
                  <td>@if($x['KLR']['6'] == 0) {{'-'}} @else {{$x['KLR']['6']}}@endif</td>
                  <td>@if($x['KMT']['1'] == 0) {{'-'}} @else {{$x['KMT']['1']}}@endif</td>
                  <td>@if($x['KMT']['2'] == 0) {{'-'}} @else {{$x['KMT']['2']}}@endif</td>
                  <td>@if($x['KMT']['3'] == 0) {{'-'}} @else {{$x['KMT']['3']}}@endif</td>
                  <td>@if($x['KMT']['4'] == 0) {{'-'}} @else {{$x['KMT']['4']}}@endif</td>
                  <td>@if($x['KMT']['5'] == 0) {{'-'}} @else {{$x['KMT']['5']}}@endif</td>
                  <td>@if($x['KMT']['6'] == 0) {{'-'}} @else {{$x['KMT']['6']}}@endif</td>
                  <td>@if($x['DTG']['1'] == 0) {{'-'}} @else {{$x['DTG']['1']}}@endif</td>
                  <td>@if($x['DTG']['2'] == 0) {{'-'}} @else {{$x['DTG']['2']}}@endif</td>
                  <td>@if($x['DTG']['3'] == 0) {{'-'}} @else {{$x['DTG']['3']}}@endif</td>
                  <td>@if($x['DTG']['4'] == 0) {{'-'}} @else {{$x['DTG']['4']}}@endif</td>
                  <td>@if($x['DTG']['5'] == 0) {{'-'}} @else {{$x['DTG']['5']}}@endif</td>
                  <td>@if($x['DTG']['6'] == 0) {{'-'}} @else {{$x['DTG']['6']}}@endif</td>
                  <td>@if($x['PRG']['1'] == 0) {{'-'}} @else {{$x['PRG']['1']}}@endif</td>
                  <td>@if($x['PRG']['2'] == 0) {{'-'}} @else {{$x['PRG']['2']}}@endif</td>
                  <td>@if($x['PRG']['3'] == 0) {{'-'}} @else {{$x['PRG']['3']}}@endif</td>
                  <td>@if($x['PRG']['4'] == 0) {{'-'}} @else {{$x['PRG']['4']}}@endif</td>
                  <td>@if($x['PRG']['5'] == 0) {{'-'}} @else {{$x['PRG']['5']}}@endif</td>
                  <td>@if($x['PRG']['6'] == 0) {{'-'}} @else {{$x['PRG']['6']}}@endif</td>
                  <td>@if($D1 == 0) {{ '-' }} @else {{ $D1 }}@endif</td>
                  <td>@if($D2 == 0) {{ '-' }} @else {{ $D2 }}@endif</td>
                  <td>@if($D3 == 0) {{ '-' }} @else {{ $D3 }}@endif</td>
                  <td>@if($D4 == 0) {{ '-' }} @else {{ $D4 }}@endif</td>
                  <td>@if($D5 == 0) {{ '-' }} @else {{ $D5 }}@endif</td>
                  <td>@if($D6 == 0) {{ '-' }} @else {{ $D6 }}@endif</td>
                  <td>@if($DT == 0) {{ '-' }} @else {{ $DT }}@endif</td>
                </tr>
                @php
                  // Total Kartu Keluarga
                  $KK = $x['KK'] + $KK;

                  // Total Data Penduduk
                  $DP1 = $x['DP']['1'] + $DP1;
                  $DP2 = $x['DP']['2'] + $DP2;
                  $DP3 = $x['DP']['3'] + $DP3;
                  $DP4 = $x['DP']['4'] + $DP4;
                  $DP5 = $x['DP']['5'] + $DP5;
                  $DP6 = $x['DP']['6'] + $DP6;

                  // Total Data Kelahiran
                  $KLR1 = $x['KLR']['1'] + $KLR1;
                  $KLR2 = $x['KLR']['2'] + $KLR2;
                  $KLR3 = $x['KLR']['3'] + $KLR3;
                  $KLR4 = $x['KLR']['4'] + $KLR4;
                  $KLR5 = $x['KLR']['5'] + $KLR5;
                  $KLR6 = $x['KLR']['6'] + $KLR6;

                  // Total Data Kelahiran
                  $KMT1 = $x['KMT']['1'] + $KMT1;
                  $KMT2 = $x['KMT']['2'] + $KMT2;
                  $KMT3 = $x['KMT']['3'] + $KMT3;
                  $KMT4 = $x['KMT']['4'] + $KMT4;
                  $KMT5 = $x['KMT']['5'] + $KMT5;
                  $KMT6 = $x['KMT']['6'] + $KMT6;

                  // Total Data Kelahiran
                  $DTG1 = $x['DTG']['1'] + $DTG1;
                  $DTG2 = $x['DTG']['2'] + $DTG2;
                  $DTG3 = $x['DTG']['3'] + $DTG3;
                  $DTG4 = $x['DTG']['4'] + $DTG4;
                  $DTG5 = $x['DTG']['5'] + $DTG5;
                  $DTG6 = $x['DTG']['6'] + $DTG6;

                  // Total Data Kelahiran
                  $PRG1 = $x['PRG']['1'] + $PRG1;
                  $PRG2 = $x['PRG']['2'] + $PRG2;
                  $PRG3 = $x['PRG']['3'] + $PRG3;
                  $PRG4 = $x['PRG']['4'] + $PRG4;
                  $PRG5 = $x['PRG']['5'] + $PRG5;
                  $PRG6 = $x['PRG']['6'] + $PRG6;

                  // Jumlah Total
                  $JTD1 = $D1 + $JTD1;
                  $JTD2 = $D2 + $JTD2;
                  $JTD3 = $D3 + $JTD3;
                  $JTD4 = $D4 + $JTD4;
                  $JTD5 = $D5 + $JTD5;
                  $JTD6 = $D6 + $JTD6;
                  $JTDT = $DT + $JTDT;
                @endphp
              @endforeach
                <tr class="total">
                  <td>Jumlah</td>
                  <td colspan="6">{{ $KK }}</td>
                  <td>{{ $DP1 }}</td>
                  <td>{{ $DP2 }}</td>
                  <td>{{ $DP3 }}</td>
                  <td>{{ $DP4 }}</td>
                  <td>{{ $DP5 }}</td>
                  <td>{{ $DP6 }}</td>
                  <td>{{ $KLR1 }}</td>
                  <td>{{ $KLR2 }}</td>
                  <td>{{ $KLR3 }}</td>
                  <td>{{ $KLR4 }}</td>
                  <td>{{ $KLR5 }}</td>
                  <td>{{ $KLR6 }}</td>
                  <td>{{ $KMT1 }}</td>
                  <td>{{ $KMT2 }}</td>
                  <td>{{ $KMT3 }}</td>
                  <td>{{ $KMT4 }}</td>
                  <td>{{ $KMT5 }}</td>
                  <td>{{ $KMT6 }}</td>
                  <td>{{ $DTG1 }}</td>
                  <td>{{ $DTG2 }}</td>
                  <td>{{ $DTG3 }}</td>
                  <td>{{ $DTG4 }}</td>
                  <td>{{ $DTG5 }}</td>
                  <td>{{ $DTG6 }}</td>
                  <td>{{ $PRG1 }}</td>
                  <td>{{ $PRG2 }}</td>
                  <td>{{ $PRG3 }}</td>
                  <td>{{ $PRG4 }}</td>
                  <td>{{ $PRG5 }}</td>
                  <td>{{ $PRG6 }}</td>
                  <td>{{ $JTD1 }}</td>
                  <td>{{ $JTD2 }}</td>
                  <td>{{ $JTD3 }}</td>
                  <td>{{ $JTD4 }}</td>
                  <td>{{ $JTD5 }}</td>
                  <td>{{ $JTD6 }}</td>
                  <td>{{ $JTDT }}</td>
                </tr>
            </tbody>
          </table>
          <div class="row">
            <div class="col-xs-12">
              <span style="font-style: italic;">Ket: 1. Islam, 2. Kristen, 3. Katolik, 4.Hindu, 5. Budha, 6. Lainnya</span>
            </div>
          </div>
          @endif
          @if($filter->type == 'Kewarganegaraan')
          <table class="table table-bordered table-striped">
            <thead>
                <tr>
                  <th rowspan="2" style="vertical-align: middle;">Kelurahan</th>
                  <th colspan="2" rowspan="2" style="vertical-align: middle;">Kartu Keluarga</th>
                  <th colspan="2">Data Penduduk</th>
                  <th colspan="2">Data Kelahiran</th>
                  <th colspan="2">Data Kematian</th>
                  <th colspan="2">Data Pindah Datang</th>
                  <th colspan="2">Data Pindah Pergi</th>
                  <th colspan="3">Jumlah</th>
                </tr>
                <tr style="font-style: italic;">
                  @for($i = 1; $i < 7; $i++)
                    <th>WNI</th>
                    <th>WNA</th>
                  @endfor
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
              @php 
                $KK = 0;
                $DPI = 0; $DPA = 0;
                $KLRI = 0; $KLRA = 0;
                $KMTI = 0; $KMTA = 0;
                $DTGI = 0; $DTGA = 0;
                $PRGI = 0; $PRGA = 0;
                $JDI = 0; $JDA = 0;
                $JDT = 0;
              @endphp
              @foreach($REKAP as $key => $x)     
                @php
                  $DI = $x['DP']['WNI'] + $x['KLR']['WNI'] + $x['KMT']['WNI'] + $x['DTG']['WNI'] + $x['PRG']['WNI'];
                  $DA = $x['DP']['WNA'] + $x['KLR']['WNA'] + $x['KMT']['WNA'] + $x['DTG']['WNA'] + $x['PRG']['WNA'];
                  $DT = $DI + $DA;
                @endphp       
                <tr>
                  <td>{{$key}}</td>
                  <td colspan="2">@if($x['KK'] == 0) {{'-'}} @else {{$x['KK']}} @endif</td>
                  <td>@if($x['DP']['WNI'] == 0) {{'-'}} @else {{$x['DP']['WNI']}} @endif</td>
                  <td>@if($x['DP']['WNA'] == 0) {{'-'}} @else {{$x['DP']['WNA']}} @endif</td>
                  <td>@if($x['KLR']['WNI'] == 0) {{'-'}} @else {{$x['KLR']['WNI']}} @endif</td>
                  <td>@if($x['KLR']['WNA'] == 0) {{'-'}} @else {{$x['KLR']['WNA']}} @endif</td>
                  <td>@if($x['KMT']['WNI'] == 0) {{'-'}} @else {{$x['KMT']['WNI']}} @endif</td>
                  <td>@if($x['KMT']['WNA'] == 0) {{'-'}} @else {{$x['KMT']['WNA']}} @endif</td>
                  <td>@if($x['DTG']['WNI'] == 0) {{'-'}} @else {{$x['DTG']['WNI']}} @endif</td>
                  <td>@if($x['DTG']['WNA'] == 0) {{'-'}} @else {{$x['DTG']['WNA']}} @endif</td>
                  <td>@if($x['PRG']['WNI'] == 0) {{'-'}} @else {{$x['PRG']['WNI']}} @endif</td>
                  <td>@if($x['PRG']['WNA'] == 0) {{'-'}} @else {{$x['PRG']['WNA']}} @endif</td>
                  <td>@if($DI == 0) {{ '-' }} @else {{ $DI }} @endif</td>
                  <td>@if($DA == 0) {{ '-' }} @else {{ $DA }} @endif</td>
                  <td>@if($DT == 0) {{ '-' }} @else {{ $DT }} @endif</td>
                </tr>
                  @php
                   $KK = $x['KK'] + $KK;
                   $DPI = $x['DP']['WNI'] + $DPI;
                   $DPA = $x['DP']['WNA'] + $DPA;
                   $KLRI = $x['KLR']['WNI'] + $KLRI;
                   $KLRA = $x['KLR']['WNA'] + $KLRA;
                   $KMTI = $x['KMT']['WNI'] + $KMTI;
                   $KMTA = $x['KMT']['WNA'] + $KMTA;
                   $DTGI = $x['DTG']['WNI'] + $DTGI;
                   $DTGA = $x['DTG']['WNA'] + $DTGA;
                   $PRGI = $x['PRG']['WNI'] + $PRGI;
                   $PRGA = $x['PRG']['WNA'] + $PRGA;
                   $JDI = $DI + $JDI;
                   $JDA = $DA + $JDA;
                   $JDT = $DT + $JDT;
                  @endphp
                @endforeach
                <tr class="total">
                  <td>Jumlah</td>
                  <td colspan="2">{{ $KK }}</td>
                  <td>{{ $DPI }}</td>
                  <td>{{ $DPA }}</td>
                  <td>{{ $KLRI }}</td>
                  <td>{{ $KLRA }}</td>
                  <td>{{ $KMTI }}</td>
                  <td>{{ $KMTA }}</td>
                  <td>{{ $DTGI }}</td>
                  <td>{{ $DTGA }}</td>
                  <td>{{ $PRGI }}</td>
                  <td>{{ $PRGA }}</td>
                  <td>{{ $JDI }}</td>
                  <td>{{ $JDA }}</td>
                  <td>{{ $JDT }}</td>
                </tr>
            </tbody>
          </table>
          @endif
        </div>
      </div>
    </div>
  </div>
@endsection