@extends('admin.layouts.app')

@section('content')
        <script src="{{URL::to('assets/js/core/jquery.countTo.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/chartjs/Chart.min.js')}}"></script>
        <!-- Page JS Code -->
        <script src="{{URL::to('assets/js/pages/base_pages_dashboard_v2.js')}}"></script>
        <script>
            jQuery(function () {
                // Init page helpers (CountTo plugin)
                App.initHelpers('appear-countTo');
            });
        </script>
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="bg-image overflow-hidden" style="background-image: url('assets/img/photos/photo31@2x.jpg');">
                    <div class="bg-black-op">
                        <div class="content content-narrow">
                            <div class="block block-transparent">
                                <div class="block-content block-content-full">
                                    <h1 class="h1 font-w300 text-white animated fadeInDown push-50-t push-5">Dashboard</h1>
                                    <h2 class="h4 font-w300 text-white-op animated fadeInUp">Selamat Datang {{Auth::user()->name}}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content content-narrow">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="block block-rounded block-opt-refresh-icon8">
                                <div class="block-header">
                                    <ul class="block-options">
                                        <li>
                                            <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                                        </li>
                                    </ul>
                                    <h3 class="block-title">Rekapitulasi Perubahan Data Bulan Ini</h3>
                                </div>
                                <div class="block-content block-content-full bg-gray-lighter text-center">
                                    <div style="height: 340px;"><canvas class="js-dash-chartjs-data"></canvas></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Stats -->
                    <div class="row text-uppercase">
                        <div class="col-xs-12 col-sm-6">
                            <a href="{{URL::to('admin/master/kartu_keluarga')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Kartu Keluarga</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$kk}}">{{$kk}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <a href="{{URL::to('admin/master/data_penduduk')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Penduduk</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$penduduk}}">{{$penduduk}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">                            
                            <a href="{{URL::to('admin/kelahiran')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Kelahiran</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$kelahiran}}">{{$kelahiran}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="{{URL::to('admin/kematian')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Kematian</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$kematian}}">{{$kematian}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="{{URL::to('admin/pindah/datang')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Pindah Datang</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$datang}}">{{$datang}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-xs-6 col-sm-3">
                            <a href="{{URL::to('admin/pindah/pergi')}}">
                                <div class="block block-rounded">
                                    <div class="block-content block-content-full">
                                        <div class="font-s12 font-w700">Data Pindah Pergi</div>
                                        <a class="h2 font-w300 text-primary" data-toggle="countTo" data-to="{{$pergi}}">{{$pergi}}</a>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- END Stats -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->
    <script>
        var BasePagesDashboardv2 = function() {
            // Chart.js Chart, for more examples you can check out http://www.chartjs.org/docs
            var initDashv2ChartJS = function(){
                // Get Chart Container
                var $dashChartDataCon = jQuery('.js-dash-chartjs-data')[0].getContext('2d');

                // Earnings Chart Data
                var $dashChartDataData= {
                    labels: ['Ancol', 'Balonggede', 'Ciateul', 'Cigereleng', 'Ciseureuh', 'Pasirluyu', 'Pungkur'],
                    datasets: [
                        {
                            label: 'Jumlah Penduduk',
                            fillColor: 'rgba(233, 30, 99, .5)',
                            strokeColor: 'rgba(233, 30, 99, .5)',
                            pointColor: 'rgba(233, 30, 99, .5)',
                            pointStrokeColor: '#fff',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(68, 180, 166, 1)',
                            data: [@foreach($cDP as $data) {{$data.', '}} @endforeach]
                        },
                        {
                            label: 'Perubahan Penduduk',
                            fillColor: 'rgba(0, 150, 136, .5)',
                            strokeColor: 'rgba(0, 150, 136, .25)',
                            pointColor: 'rgba(0, 150, 136, .25)',
                            pointStrokeColor: '#fff',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(68, 180, 166, 1)',
                            data: [@foreach($c as $data) {{$data.', '}} @endforeach]
                        },
                        {
                            label: 'Kartu Keluarga',
                            fillColor: 'rgba(57, 73, 171, .5)',
                            strokeColor: 'rgba(57, 73, 171, .25)',
                            pointColor: 'rgba(57, 73, 171, .25)',
                            pointStrokeColor: '#fff',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(68, 180, 166, 1)',
                            data: [@foreach($cKK as $data) {{$data.', '}} @endforeach]
                        }
                    ]
                };

                // Init Earnings Chart
                var $dashChartData = new Chart($dashChartDataCon).Line($dashChartDataData, {
                    scaleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
                    scaleFontColor: '#999',
                    scaleFontStyle: '600',
                    tooltipTitleFontFamily: "'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif",
                    tooltipCornerRadius: 3,
                    maintainAspectRatio: false,
                    responsive: true
                });
            };

            return {
                init: function () {
                    initDashv2ChartJS();
                }
            };
        }();

        // Initialize when page loads
        jQuery(function(){ BasePagesDashboardv2.init(); });
    </script>
@endsection