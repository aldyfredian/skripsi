@extends('admin.layouts.app')

@section('content')
@if(Auth::user()->role == 'Admin')
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.css')}}">
        <script src="{{URL::to('assets/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::to('assets/js/pages/base_tables_datatables.js')}}"></script>

            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Header -->
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                User Management
                            </h1>
                            <ol class="breadcrumb push-10-t">
                                <li><a href="{{URL::to('/')}}" class="link-effect">Home</a></li>
                                <li>User Management</li>
                            </ol>
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <a data-toggle="modal" data-target="#modal-popin" type="button" class="btn btn-info btn-rounded">
                                <i class="fa fa-filter"></i> Filter Data
                            </a>
                            <a href="{{URL::to('admin/user_management/create')}}" class="btn btn-primary btn-rounded">
                                <i class="fa fa-plus"></i> Tambah Data
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    <!-- Dynamic Table Full -->
                    <div class="block">
                        <div class="block-content">
                            <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/base_tables_datatables.js -->
                            <table class="table table-bordered table-striped" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th width="150px">Aksi</th>
                                    </tr>
                                </thead>

                                {{-- Data Loaded Through AJAX --}}
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END Dynamic Table Full -->
                </div>
                <!-- END Page Content -->
            </main>
            <!-- END Main Container -->

    {!! Form::open([
            'method' => 'delete', 
            'route' => ['user_management.destroy', null],
            'class' => 'hiddenDeleteForm'
    ])!!}
    {!! Form::close() !!}

    <!-- Modal -->
    <div class="modal fade" id="modal-popin" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popin">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-header bg-primary-dark">
                        <ul class="block-options">
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>
                        <h3 class="block-title">Filter Data</h3>
                    </div>
                    <div class="block-content">
                        {!! Form::open(['class' => 'form-horizontal', 'id'=> 'filter-form']) !!}
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('nama', null, ['id'=>'filter-nama', 'class' => 'form-control']) !!}
                                    <label>Nama</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('username', null, ['id'=>'filter-username', 'class' => 'form-control']) !!}
                                    <label>Username</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::text('email', null, ['id'=>'filter-email', 'class' => 'form-control']) !!}
                                    <label>Email</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-material floating">
                                    {!! Form::select('role', ['Semua' =>'Semua', 'Admin' => 'Admin', 'Operator' => 'Operator'], null,['id'=>'filter-role', 'class' => 'form-control', 'placeholder' => '']) !!}
                                    <label>Role</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-sm btn-primary" type="submit" date-dismiss="modal">Filter</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <!-- END Pop In Modal -->

  <script type="text/javascript">
    $(document).ready(function() {
        var t = $('#dataTable').DataTable( {
            ordering: false,
            searching: false,
            responsive: true,
            language: {
              emptyTable: "Tidak ada data",
              sSearchPlaceholder: 'Search..',
              lengthMenu: '_MENU_',
              search: '_INPUT_',
              paginate: {
                previous: '<i class="icon wb-chevron-left-mini"></i>',
                next: '<i class="icon wb-chevron-right-mini"></i>'
              }
            },
            columns: [
                { "data": "name" },
                { "data": "username" },
                { "data": "email" },
                { "data": "role" },
                { "data": "action" }
            ],
            processing: true,
            serverSide: true,
            ajax: {
              "url": '{{ URL::action('UserController@getDatatableUsers') }}',
              "type": "POST",
              "data":  function(d) {
                   d.nama = $('#filter-nama').val(),
                   d.username = $('#filter-username').val(),
                   d.email = $('#filter-email').val(),
                   d.role = $('#filter-role').val()
              }
            },
            deferRender: true
        });

        // Submit Filter
        $('#filter-form').submit(function(e) {
            e.preventDefault();
            t.draw();
        });
        
        $('#dataTable').on( 'draw.dt', function () {
          $('.deleteButton').on("click", function(e) {
            e.preventDefault();
            var deleteId = $(this).attr('deleteId');

            swal({
                title: "Hapus data ini?",
                text: "Data tidak dapat dikembalikan",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#D32F2F',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Tidak, Batalkan.',
                cancelButtonColor: '#BDBDBD',
                closeOnConfirm: false,
                closeOnCancel: true
              }).then(function () {
                  $('.hiddenDeleteForm').attr('action',"{{ action('UserController@destroy', null) }}/"+deleteId)
                  $('.hiddenDeleteForm').submit();
              })
          });
        });
    });
  </script>
@else
@php print_r("Anda Tidak Memiliki Hak Akses Sebagai Admin!"); exit; @endphp
@endif
@endsection