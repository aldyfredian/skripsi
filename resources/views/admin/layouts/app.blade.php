<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Sistem Informasi Data Kependudukan</title>

        <meta name="description" content="OneUI - Admin Dashboard Template &amp; UI Framework created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{URL::to('assets/img/favicons/favicon.png')}}">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{URL::to('assets/js/plugins/sweetalert2/sweetalert2.min.css')}}">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="{{URL::to('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" id="css-main" href="{{URL::to('assets/css/oneui.css')}}">
        <link rel="stylesheet" href="{{URL::to('assets/css/custom.css')}}">

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js')}} -->
        <script src="{{URL::to('assets/js/core/jquery.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/bootstrap.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/jquery.slimscroll.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/jquery.scrollLock.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/jquery.appear.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/jquery.placeholder.min.js')}}"></script>
        <script src="{{URL::to('assets/js/core/js.cookie.min.js')}}"></script>
        <script src="{{URL::to('assets/js/app.js')}}"></script>
    </head>
    <body>
    @if (Session::has('status'))
        @php
          $message = session('status');
        @endphp
        <script type="text/javascript">
          $(document).ready(function() {

            setTimeout(function() {
              swal({
                title: "{{ $message['title'] }}",
                text: "{{ $message['text'] }}",
                type: "{{ $message['type'] }}",
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Ok',
                closeOnConfirm: true
              });
            }, 500);
            
          });
        </script>
    @endif

        @include('admin.layouts.sidebar')
        @include('admin.layouts.header')

        @yield('content')

        @include('admin.layouts.footer')

        <!-- Page Plugins -->
        <script src="{{URL::to('assets/js/plugins/sweetalert2/es6-promise.auto.min.js')}}"></script>
        <script src="{{URL::to('assets/js/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

    </body>
</html>