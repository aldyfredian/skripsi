<!DOCTYPE html>
<html>
<head>
	<title>Print Data</title>
    <link rel="stylesheet" href="{{URL::to('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/print.css')}}">
    <style type="text/css">
    	h1, h2, h3, h4, h5, h6{
    		margin: 5px 0;
    	}
    	.logo-pemkot{
    		position: absolute;
    		left: 5px;
    		top: 5px;
    		width: 60px;
    	}
    </style>
</head>
<body>
	<div class="container-fluid">

		@include('admin.layouts.print.header')

		@yield('printBody')

		@include('admin.layouts.print.footer')
		
	</div>
</body>
</html>