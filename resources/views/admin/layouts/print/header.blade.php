
  <div class="row" style="margin: 25px 0;">
    <div class="col-xs-12" style="text-align: center;">
      <div class="logo-pemkot">
        <img src="{{URL::asset('assets/img/logo-pemkot.jpg')}}" style="width: 125px;">
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h3>PEMERINTAH KOTA BANDUNG</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h4>KECAMATAN REGOL</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <h5>Jl. Denki No.54 Ciseureuh, Regol. Telp: (022) 520 2489</h5>
        </div>
      </div>
    </div>
  </div>
  <hr>
    <div class="row">
      <div class="col-xs-12">
        <h4 style="text-transform: uppercase; text-align: center; margin: 10px 0;">LAPORAN {{ $typeLaporan }}</h4>
      </div>
    </div>