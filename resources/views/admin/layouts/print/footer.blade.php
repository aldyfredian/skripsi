
      <div class="row">
        <div class="col-xs-2 col-xs-offset-10">
          <div class="row">
            @php Carbon\Carbon::setlocale('Asia/Jakarta'); @endphp
            <div class="col-xs-12">Bandung, {{ date("d F Y", strtotime(Carbon\Carbon::now())) }}</div>
          </div>
          <div class="row">
            <div class="col-xs-12"><span style="font-weight: bold; text-transform: uppercase;">Camat Regol</span></div>
          </div>
          <div class="row" style="margin-top: 100px;">
            <div class="col-xs-12">
              <span style="font-weight: bold;">Drs. Iwan Sumaryana, M.M.</span>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12"><span>NIP: 196801251989031004 </span></div>
          </div>
        </div>
      </div>