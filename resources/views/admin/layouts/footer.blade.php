    <!-- Footer -->
    <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
        <div class="pull-right">
            Dibuat untuk Kec. Regol Kota Bandung
        </div>
        <div class="pull-left">
           Aldy Fredian &copy; 2017. Memenuhi Tugas Akhir / Skripsi
        </div>
    </footer>
    <!-- END Footer -->
</div>
<!-- END Page Container -->